package staff

import (
	staffPb "bitbucket.org/canopei/staff/protobuf"
)

// PublicStaff is the public representation of the Staff type
type PublicStaff struct {
	UUID      string   `json:"uuid"`
	Name      string   `json:"staffName"`
	FirstName string   `json:"staffFirstName"`
	LastName  string   `json:"staffLastName"`
	Gender    int32    `json:"gender"`
	Featured  int32    `json:"featured"`
	CityNA    []string `json:"cityNA"`
	NameNA    string   `json:"staffNameNA"`
}

// GetPublicStaff creates a PublicStaff from a Staff object
func GetPublicStaff(staff *staffPb.Staff) *PublicStaff {
	return &PublicStaff{
		UUID:      staff.Uuid,
		Name:      staff.Name,
		FirstName: staff.FirstName,
		LastName:  staff.LastName,
		Gender:    staff.Gender,
		Featured:  staff.Featured,
		CityNA:    staff.Cities,
		NameNA:    staff.Name,
	}
}
