package staff

import (
	"context"
	"encoding/json"
	"fmt"

	classPb "bitbucket.org/canopei/class/protobuf"
	queueHelper "bitbucket.org/canopei/golibs/queue"
	"bitbucket.org/canopei/golibs/slices"
	sitePb "bitbucket.org/canopei/site/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
	"gopkg.in/olivere/elastic.v5"
)

// ReindexService is the implementation of a reindex service (from DB to ES)
type ReindexService struct {
	Logger               *logrus.Entry
	Queue                *nsq.Producer
	ElasticBulkProcessor *elastic.BulkProcessor
	StaffService         staffPb.StaffServiceClient
	SiteService          sitePb.SiteServiceClient
	ClassService         classPb.ClassServiceClient
}

// NewReindexService creates a new ReindexService instance
func NewReindexService(
	logger *logrus.Entry,
	queue *nsq.Producer,
	elasticBulkProcessor *elastic.BulkProcessor,
	staffService staffPb.StaffServiceClient,
	siteService sitePb.SiteServiceClient,
	classService classPb.ClassServiceClient,
) *ReindexService {
	return &ReindexService{
		Logger:               logger,
		Queue:                queue,
		ElasticBulkProcessor: elasticBulkProcessor,
		StaffService:         staffService,
		SiteService:          siteService,
		ClassService:         classService,
	}
}

// ReindexSiteStaff indexes a site's staff from DB to ES
func (s *ReindexService) ReindexSiteStaff(siteUUID string, indexName string) error {
	ctx := context.Background()

	dbSite, err := s.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		s.Logger.Errorf("Cannot fetch site by UUIDs '%s': %v", siteUUID, err)
		return fmt.Errorf("Cannot fetch site by UUID '%s': %v", siteUUID, err)
	}

	result, err := s.StaffService.GetStaffBySite(ctx, &staffPb.GetStaffBySiteRequest{SiteId: dbSite.Id})
	if err != nil {
		s.Logger.Errorf("Cannot fetch the staff for site '%s': %v", dbSite.Uuid, err)
		return fmt.Errorf("Cannot fetch the staff for site '%s': %v", dbSite.Uuid, err)
	}
	dbStaff := result.Staff

	s.Logger.Infof("Reindexing %d staff for site '%s'.", len(dbStaff), dbSite.Uuid)

	// Fetch the staff location IDs based on their class schedule
	staffIDsToLocationIDsMap := map[int32][]int32{}
	staffIDs := []int32{}
	for _, dbOneStaff := range dbStaff {
		staffIDs = append(staffIDs, dbOneStaff.Id)
	}
	staffIDsWithLocationIDs, err := s.ClassService.GetStaffLocationIDsByStaffIDs(ctx, &classPb.GetStaffLocationIDsByStaffIDsRequest{
		StaffIds: staffIDs,
	})
	if err != nil {
		s.Logger.Errorf("Cannot fetch the staff location IDs for site '%s': %v", dbSite.Uuid, err)
		return fmt.Errorf("Cannot fetch the staff location IDs for site '%s': %v", dbSite.Uuid, err)
	}
	for _, staffIDWithLocationIDs := range staffIDsWithLocationIDs.Staff {
		staffIDsToLocationIDsMap[staffIDWithLocationIDs.StaffId] = staffIDWithLocationIDs.LocationIds
	}

	// Fetch the full locations
	locationsMap := map[int32]*sitePb.Location{}
	locationIDs := []int32{}
	for _, staffLocationIDs := range staffIDsToLocationIDsMap {
		for _, staffLocationID := range staffLocationIDs {
			if !slices.Icontains32(locationIDs, staffLocationID) {
				locationIDs = append(locationIDs, staffLocationID)
			}
		}
	}
	if len(locationIDs) > 0 {
		locationsResponse, err := s.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{Ids: locationIDs})
		if err != nil {
			s.Logger.Errorf("Cannot fetch the locations by IDs for site '%s': %v", dbSite.Uuid, err)
			return fmt.Errorf("Cannot fetch the locations by IDs for site '%s': %v", dbSite.Uuid, err)
		}
		for _, dbLocation := range locationsResponse.Locations {
			locationsMap[dbLocation.Id] = dbLocation
		}
	}

	staffIDToCitiesMap := map[int32][]string{}
	for staffID, locationIDs := range staffIDsToLocationIDsMap {
		cities := []string{}
		for _, locationID := range locationIDs {
			if location, ok := locationsMap[locationID]; ok {
				if location.CityGroupName != "" {
					cities = append(cities, location.CityGroupName)
				}
			}
		}
		staffIDToCitiesMap[staffID] = cities
	}

	for _, dbOneStaff := range dbStaff {
		if dbOneStaff.IsApproved && dbOneStaff.MbId > 0 {
			dbOneStaff.Cities = []string{}
			if cities, ok := staffIDToCitiesMap[dbOneStaff.Id]; ok {
				dbOneStaff.Cities = cities
			}

			r := elastic.NewBulkIndexRequest().
				Index(indexName).
				Type("staff").
				Parent(dbSite.Uuid).
				Id(dbOneStaff.Uuid).
				Doc(GetPublicStaff(dbOneStaff))
			s.ElasticBulkProcessor.Add(r)
		}
	}

	s.ElasticBulkProcessor.Flush()

	// Add to the queue a finished message for this site (only for the staff component)
	messageData := queueHelper.NewReindexSiteStaffMessage(siteUUID, indexName, queueHelper.ReindexStatusDone)
	message, _ := json.Marshal(messageData)
	s.Queue.Publish("reindex", message)

	return nil
}
