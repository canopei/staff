package staff

import (
	"context"
	"fmt"
	"log"
	"strings"

	classPb "bitbucket.org/canopei/class/protobuf"
	"bitbucket.org/canopei/golibs/elasticsearch"
	"bitbucket.org/canopei/golibs/slices"
	mbHelpers "bitbucket.org/canopei/mindbody/helpers"
	staffMb "bitbucket.org/canopei/mindbody/services/staff"
	sitePb "bitbucket.org/canopei/site/protobuf"
	"bitbucket.org/canopei/staff/config"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	nsq "github.com/nsqio/go-nsq"
)

// SyncService is the implementation of a sync service
type SyncService struct {
	Logger               *logrus.Entry
	StaffMBClient        *staffMb.Staff_x0020_ServiceSoap
	Queue                *nsq.Producer
	ElasticsearchService *elasticsearch.Service
	StaffService         staffPb.StaffServiceClient
	SiteService          sitePb.SiteServiceClient
	ClassService         classPb.ClassServiceClient
	MindbodyConfig       *config.MindbodyConfig
}

// NewSyncService creates a new SyncService instance
func NewSyncService(
	logger *logrus.Entry,
	staffMBClient *staffMb.Staff_x0020_ServiceSoap,
	queue *nsq.Producer,
	elasticsearchService *elasticsearch.Service,
	staffService staffPb.StaffServiceClient,
	siteService sitePb.SiteServiceClient,
	classService classPb.ClassServiceClient,
	mindbodyConfig *config.MindbodyConfig,
) *SyncService {
	return &SyncService{
		Logger:               logger,
		StaffMBClient:        staffMBClient,
		Queue:                queue,
		ElasticsearchService: elasticsearchService,
		StaffService:         staffService,
		SiteService:          siteService,
		ClassService:         classService,
		MindbodyConfig:       mindbodyConfig,
	}
}

// SyncSiteStaff synchronizes the staff members for the given site (UUID) to DB and ES from MB
func (ss *SyncService) SyncSiteStaff(siteUUID string) error {
	ss.Logger.Debugf("Will sync site UUID: %s", siteUUID)

	ctx := context.Background()

	// Get the site from DB
	dbSite, err := ss.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Uuid: siteUUID})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the site by UUIDs: %v", err)
		return fmt.Errorf("Cannot fetch the site by UUIDs: %v", err)
	}

	ss.Logger.Debugf("Syncing staff for site MB ID %d.", dbSite.MbId)

	// Get the staff from Mindbody
	mbRequest := mbHelpers.CreateStaffMBRequest([]int32{dbSite.MbId}, ss.MindbodyConfig.SourceName, ss.MindbodyConfig.SourcePassword)
	mbStaffResponse, err := ss.StaffMBClient.GetStaff(&staffMb.GetStaff{Request: &staffMb.GetStaffRequest{
		MBRequest: mbRequest,
	}})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the staff from Mindbody: %v", err)
		return fmt.Errorf("Cannot fetch the staff from Mindbody: %v", err)
	}
	mbStaffResult := mbStaffResponse.GetStaffResult
	if mbStaffResult.ErrorCode.Int != 200 {
		ss.Logger.Errorf("Mindbody API response error: %s", mbStaffResult.Message)
		return fmt.Errorf("Mindbody API response error: %s", mbStaffResult.Message)
	}

	ss.Logger.Debugf("Found %d staff members in Mindbody.", len(mbStaffResult.StaffMembers.Staff))
	if len(mbStaffResult.StaffMembers.Staff) == 0 {
		ss.Logger.Info("No staff members were found.")
	}

	ss.Logger.Debugf("Syncing %d staff members...", len(mbStaffResult.StaffMembers.Staff))

	// Fetch the staff from DB
	dbStaffMembersResult, err := ss.StaffService.GetStaffBySite(ctx, &staffPb.GetStaffBySiteRequest{SiteId: dbSite.Id})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch staff members from DB: %v", err)
		return fmt.Errorf("Cannot fetch staff members from DB: %v", err)
	}
	ss.Logger.Debugf("Got %d staff members from DB before update.", len(dbStaffMembersResult.Staff))

	dbStaffMembers := map[int64]*staffPb.Staff{}
	for _, v := range dbStaffMembersResult.Staff {
		dbStaffMembers[v.MbId] = v
	}

	// Fetch the staff location IDs based on their class schedule
	staffIDsToLocationIDsMap := map[int32][]int32{}
	staffIDs := []int32{}
	for _, dbOneStaff := range dbStaffMembers {
		staffIDs = append(staffIDs, dbOneStaff.Id)
	}
	staffIDsWithLocationIDs, err := ss.ClassService.GetStaffLocationIDsByStaffIDs(ctx, &classPb.GetStaffLocationIDsByStaffIDsRequest{
		StaffIds: staffIDs,
	})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch the staff location IDs for site '%s': %v", dbSite.Uuid, err)
		return fmt.Errorf("Cannot fetch the staff location IDs for site '%s': %v", dbSite.Uuid, err)
	}
	for _, staffIDWithLocationIDs := range staffIDsWithLocationIDs.Staff {
		staffIDsToLocationIDsMap[staffIDWithLocationIDs.StaffId] = staffIDWithLocationIDs.LocationIds
	}
	// Fetch the full locations
	locationsMap := map[int32]*sitePb.Location{}
	locationIDs := []int32{}
	for _, staffLocationIDs := range staffIDsToLocationIDsMap {
		for _, staffLocationID := range staffLocationIDs {
			if !slices.Icontains32(locationIDs, staffLocationID) {
				locationIDs = append(locationIDs, staffLocationID)
			}
		}
	}
	if len(locationIDs) > 0 {
		locationsResponse, err := ss.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{Ids: locationIDs})
		if err != nil {
			ss.Logger.Errorf("Cannot fetch the locations by IDs for site '%s': %v", dbSite.Uuid, err)
			return fmt.Errorf("Cannot fetch the locations by IDs for site '%s': %v", dbSite.Uuid, err)
		}
		for _, dbLocation := range locationsResponse.Locations {
			locationsMap[dbLocation.Id] = dbLocation
		}
	}
	staffIDToCitiesMap := map[int32][]string{}
	for staffID, locationIDs := range staffIDsToLocationIDsMap {
		cities := []string{}
		for _, locationID := range locationIDs {
			if location, ok := locationsMap[locationID]; ok {
				if location.CityGroupName != "" {
					cities = append(cities, location.CityGroupName)
				}
			}
		}
		staffIDToCitiesMap[staffID] = cities
	}

	existingStaff := map[int64]bool{}
	for _, mbStaff := range mbStaffResult.StaffMembers.Staff {
		existingStaff[mbStaff.ID.Int] = true
		indexToES := false

		dbStaff, ok := dbStaffMembers[mbStaff.ID.Int]
		if !ok {
			ss.Logger.Infof("Adding new staff MB ID %d in DB.", mbStaff.ID.Int)

			// New staff, add to DB
			newStaff := mbStaffToPbStaff(mbStaff)
			newStaff.SiteId = dbSite.Id

			dbStaff, err = ss.StaffService.CreateStaff(context.Background(), newStaff)
			if err != nil {
				ss.Logger.Errorf("Cannot create staff in DB: %v", err)
				continue
			}
			indexToES = true
		} else {
			currentStaff := mbStaffToPbStaff(mbStaff)

			if hasStaffChanged(dbStaff, currentStaff) {
				ss.Logger.Infof("Updating staff MB ID %d (%s) in DB.", mbStaff.ID.Int, dbStaff.Uuid)

				currentStaff.Uuid = dbStaff.Uuid
				currentStaff.Featured = -1
				currentStaff.IsApproved = true
				// "-" means do not update
				currentStaff.Quote = "-"

				dbStaff, err = ss.StaffService.UpdateStaff(context.Background(), currentStaff)
				if err != nil {
					ss.Logger.Errorf("Cannot update staff '%s' in DB: %v", currentStaff.Uuid, err)
					continue
				}
				indexToES = true
			}
		}

		if dbStaff.MbId <= 0 {
			indexToES = false
		}

		if indexToES {
			dbStaff.Cities = []string{}
			if cities, ok := staffIDToCitiesMap[dbStaff.Id]; ok {
				dbStaff.Cities = cities
			}

			// We will PUT this in Elasticsearch
			ss.Logger.Debugf("Sending in ES staff %#v", dbStaff)

			_, err = indexStaffToES(ss.ElasticsearchService, "sng", dbSite.Uuid, dbStaff)
			if err != nil {
				ss.Logger.Errorf("Failed to update staff '%s' in ES: %v", dbStaff.Uuid, err)
				continue
			}
		}
	}

	// Delete untouched staff
	for mbID, dbStaff := range dbStaffMembers {
		if _, ok := existingStaff[mbID]; !ok {
			_, err = deleteStaffFromES(ss.ElasticsearchService, "sng", dbSite.Uuid, dbStaff.Uuid)
			if err != nil {
				ss.Logger.Errorf("Failed to delete staff '%s' from ES: %v", dbStaff.Uuid, err)
			}

			_, err := ss.StaffService.DeleteStaff(context.Background(), &staffPb.DeleteStaffRequest{Uuid: dbStaff.Uuid})
			if err != nil {
				ss.Logger.Errorf("Cannot delete staff '%s' from DB: %v", dbStaff.Uuid, err)
			}
		}
	}

	return nil
}

// SyncStaffFromDB synchronizes the given staff (UUIDs) to ES from DB
func (ss *SyncService) SyncStaffFromDB(staffUUIDs []string) ([]string, error) {
	var messagesToFinish []string
	var dbStaff []*staffPb.Staff
	var err error

	ctx := context.Background()

	// Get the locations from DB
	response, err := ss.StaffService.GetStaff(ctx, &staffPb.GetStaffRequest{Uuids: strings.Join(staffUUIDs, ",")})
	if err != nil {
		ss.Logger.Errorf("Cannot fetch staff by UUIDs: %v", err)
		return nil, fmt.Errorf("Cannot fetch staff by UUIDs: %v", err)
	}

	dbStaff = response.Staff

	ss.Logger.Debugf("Syncing %d staff members from DB. Requested %d staff members.", len(dbStaff), len(staffUUIDs))

	for _, staff := range dbStaff {
		messagesToFinish = append(messagesToFinish, staff.Uuid)

		if staff.MbId <= 0 {
			continue
		}

		// get the parent Site
		dbSite, err := ss.SiteService.GetSite(ctx, &sitePb.GetSiteRequest{Id: staff.SiteId})
		if err != nil {
			ss.Logger.Errorf("Failed to fetch staff's '%s' site from DB: %v", staff.Uuid, err)
			continue
		}

		staff.Cities = []string{}
		staffIDsWithLocationIDs, err := ss.ClassService.GetStaffLocationIDsByStaffIDs(ctx, &classPb.GetStaffLocationIDsByStaffIDsRequest{
			StaffIds: []int32{staff.Id},
		})
		if err != nil {
			ss.Logger.Errorf("Cannot fetch the staff location IDs for staff ID %d: %v", staff.Id, err)
			continue
		}
		if len(staffIDsWithLocationIDs.Staff) > 0 {
			locationsResponse, err := ss.SiteService.GetLocations(ctx, &sitePb.GetLocationsRequest{
				Ids: staffIDsWithLocationIDs.Staff[0].LocationIds,
			})
			if err != nil {
				ss.Logger.Errorf("Cannot fetch the locations by IDs for staff ID %d: %v", staff.Id, err)
				continue
			}
			log.Println(locationsResponse.Locations)
			for _, dbLocation := range locationsResponse.Locations {
				if dbLocation.CityGroupName != "" {
					staff.Cities = append(staff.Cities, dbLocation.CityGroupName)
				}
			}
		}

		_, err = indexStaffToES(ss.ElasticsearchService, "sng", dbSite.Uuid, staff)
		if err != nil {
			ss.Logger.Errorf("Failed to update staff '%s' in ES: %v", staff.Uuid, err)
			continue
		}
	}

	return messagesToFinish, nil
}
