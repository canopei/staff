package staff

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"bitbucket.org/canopei/mindbody"
	staffMb "bitbucket.org/canopei/mindbody/services/staff"
	staffPb "bitbucket.org/canopei/staff/protobuf"
)

// Just a simple sanity check
func TestHasSiteChanged(t *testing.T) {
	assert := assert.New(t)

	// Simple
	dbStaff := &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{}}
	mbStaff := &staffPb.Staff{Name: "bar", Address: &staffPb.StaffAddress{}}
	assert.True(hasStaffChanged(dbStaff, mbStaff))

	dbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{}}
	mbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{}}
	assert.False(hasStaffChanged(dbStaff, mbStaff))

	// With Address
	dbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}}
	mbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}}
	assert.False(hasStaffChanged(dbStaff, mbStaff))

	dbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}}
	mbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "changed"}}
	assert.True(hasStaffChanged(dbStaff, mbStaff))

	// With phones
	dbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}, Phones: []*staffPb.StaffPhone{
		&staffPb.StaffPhone{TypeName: "home", Number: "123"},
		&staffPb.StaffPhone{TypeName: "work", Number: "124"},
		&staffPb.StaffPhone{TypeName: "mobile", Number: "125"},
	}}
	mbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}, Phones: []*staffPb.StaffPhone{
		&staffPb.StaffPhone{TypeName: "home", Number: "123"},
		&staffPb.StaffPhone{TypeName: "work", Number: "124"},
		&staffPb.StaffPhone{TypeName: "mobile", Number: "125"},
	}}
	assert.False(hasStaffChanged(dbStaff, mbStaff))

	dbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}, Phones: []*staffPb.StaffPhone{
		&staffPb.StaffPhone{TypeName: "home", Number: "123"},
		&staffPb.StaffPhone{TypeName: "work", Number: "124"},
		&staffPb.StaffPhone{TypeName: "mobile", Number: "125"},
	}}
	mbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}, Phones: []*staffPb.StaffPhone{
		&staffPb.StaffPhone{TypeName: "home", Number: "123"},
		&staffPb.StaffPhone{TypeName: "work", Number: "1245"},
		&staffPb.StaffPhone{TypeName: "mobile", Number: "125"},
	}}
	assert.True(hasStaffChanged(dbStaff, mbStaff))

	dbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}, Phones: []*staffPb.StaffPhone{
		&staffPb.StaffPhone{TypeName: "home", Number: "123"},
		&staffPb.StaffPhone{TypeName: "work", Number: "124"},
		&staffPb.StaffPhone{TypeName: "mobile", Number: "125"},
	}}
	mbStaff = &staffPb.Staff{Name: "foo", Address: &staffPb.StaffAddress{Address: "fooaddr"}, Phones: []*staffPb.StaffPhone{
		&staffPb.StaffPhone{TypeName: "home", Number: "123"},
		&staffPb.StaffPhone{TypeName: "mobile", Number: "125"},
	}}
	assert.True(hasStaffChanged(dbStaff, mbStaff))
}

// Test the gender conversion and the phones construction
func TestMBStaffToPbStaff(t *testing.T) {
	assert := assert.New(t)

	mbStaff := &staffMb.Staff{
		ID:          &mindbody.CustomInt64{Int: 123},
		Email:       "foo@bar.com",
		MobilePhone: "12345",
		HomePhone:   "67890",
		IsMale:      false,
		City:        "city",
		Address2:    "address2",
	}

	pbStaff := mbStaffToPbStaff(mbStaff)

	assert.Len(pbStaff.Phones, 2)
	// This depends on order - but is easier to check it this way!
	assert.Equal(pbStaff.Phones[1].TypeName, "mobile")
	assert.Equal(pbStaff.Phones[1].Number, "12345")
	assert.Equal(pbStaff.Address.City, "city")
	assert.Equal(pbStaff.Address.Address2, "address2")
}
