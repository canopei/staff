package staff

import (
	"context"
	"fmt"

	elastic "gopkg.in/olivere/elastic.v5"

	"bitbucket.org/canopei/golibs/elasticsearch"
	staffMb "bitbucket.org/canopei/mindbody/services/staff"
	staffPb "bitbucket.org/canopei/staff/protobuf"
)

func findStaffByMBID(staffs []*staffPb.Staff, mbID int64) (*staffPb.Staff, error) {
	for _, staff := range staffs {
		if staff.MbId == mbID {
			return staff, nil
		}
	}
	return nil, fmt.Errorf("Staff was not found")
}

func hasStaffChanged(dbStaff *staffPb.Staff, mbStaff *staffPb.Staff) bool {
	return dbStaff.Name != mbStaff.Name ||
		dbStaff.FirstName != mbStaff.FirstName ||
		dbStaff.LastName != mbStaff.LastName ||
		dbStaff.Bio != mbStaff.Bio ||
		dbStaff.Gender != mbStaff.Gender ||
		dbStaff.ImageUrl != mbStaff.ImageUrl ||
		dbStaff.Email != mbStaff.Email ||
		dbStaff.MultiLocation != mbStaff.MultiLocation ||
		dbStaff.AppointmentTrn != mbStaff.AppointmentTrn ||
		dbStaff.ReservationTrn != mbStaff.ReservationTrn ||
		dbStaff.IndependentContractor != mbStaff.IndependentContractor ||
		dbStaff.AlwaysAllowDoubleBooking != mbStaff.AlwaysAllowDoubleBooking ||
		dbStaff.UserAccessLevel != mbStaff.UserAccessLevel ||
		hasAddressChanged(dbStaff.Address, mbStaff.Address) ||
		havePhonesChanged(dbStaff.Phones, mbStaff.Phones)
}

func hasAddressChanged(dbAddress *staffPb.StaffAddress, mbAddress *staffPb.StaffAddress) bool {
	return dbAddress.Address != mbAddress.Address ||
		dbAddress.Address2 != mbAddress.Address2 ||
		dbAddress.City != mbAddress.City ||
		dbAddress.Country != mbAddress.Country ||
		dbAddress.ForeignZip != mbAddress.ForeignZip ||
		dbAddress.State != mbAddress.State ||
		dbAddress.PostalCode != mbAddress.PostalCode
}

func havePhonesChanged(dbPhones []*staffPb.StaffPhone, mbPhones []*staffPb.StaffPhone) bool {
	for _, dbPh := range dbPhones {
		found := false
		for _, mbPh := range mbPhones {

			if dbPh.TypeName == mbPh.TypeName {
				if dbPh.Number != mbPh.Number {
					return true
				}

				found = true
			}
		}
		if !found {
			return true
		}
	}
	return false
}

func mbStaffToPbStaff(mbStaff *staffMb.Staff) *staffPb.Staff {
	gender := int32(2)
	if !mbStaff.IsMale {
		gender = 1
	}

	phones := []*staffPb.StaffPhone{}

	if mbStaff.HomePhone != "" {
		phones = append(phones, &staffPb.StaffPhone{TypeName: "home", Number: mbStaff.HomePhone})
	}
	if mbStaff.WorkPhone != "" {
		phones = append(phones, &staffPb.StaffPhone{TypeName: "work", Number: mbStaff.WorkPhone})
	}
	if mbStaff.MobilePhone != "" {
		phones = append(phones, &staffPb.StaffPhone{TypeName: "mobile", Number: mbStaff.MobilePhone})
	}

	return &staffPb.Staff{
		MbId:                     mbStaff.ID.Int,
		Name:                     mbStaff.Name,
		FirstName:                mbStaff.FirstName,
		LastName:                 mbStaff.LastName,
		Bio:                      mbStaff.Bio,
		Gender:                   gender,
		ImageUrl:                 mbStaff.ImageURL,
		Email:                    mbStaff.Email,
		MultiLocation:            mbStaff.MultiLocation,
		AppointmentTrn:           mbStaff.AppointmentTrn,
		ReservationTrn:           mbStaff.ReservationTrn,
		IndependentContractor:    mbStaff.IndependentContractor,
		AlwaysAllowDoubleBooking: mbStaff.AlwaysAllowDoubleBooking,
		UserAccessLevel:          mbStaff.UserAccessLevel,

		Address: &staffPb.StaffAddress{
			Address:    mbStaff.Address,
			Address2:   mbStaff.Address2,
			City:       mbStaff.City,
			State:      mbStaff.State,
			Country:    mbStaff.Country,
			PostalCode: mbStaff.PostalCode,
			ForeignZip: mbStaff.ForeignZip,
		},

		Phones: phones,
	}
}

func indexStaffToES(esService *elasticsearch.Service, indexName string, parentSiteUUID string, dbStaff *staffPb.Staff) (*elastic.IndexResponse, error) {
	return esService.Client.
		Index().
		Index(indexName).
		Type("staff").
		Parent(parentSiteUUID).
		Id(dbStaff.Uuid).
		BodyJson(GetPublicStaff(dbStaff)).
		Do(context.Background())
}

func deleteStaffFromES(esService *elasticsearch.Service, indexName string, parentSiteUUID string, dbStaffUUID string) (*elastic.DeleteResponse, error) {
	return esService.Client.
		Delete().
		Index(indexName).
		Type("staff").
		Parent(parentSiteUUID).
		Id(dbStaffUUID).
		Do(context.Background())
}
