package main

import (
	"context"
	"encoding/json"
	"net/http"
	"time"

	accountPb "bitbucket.org/canopei/account/protobuf"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// StaffRequestsHandler handles staff related requests
type StaffRequestsHandler struct {
	Logger        *logrus.Entry
	AccountClient accountPb.AccountServiceClient
	SelfClient    staffPb.StaffServiceClient
}

// NewStaffRequestHandler creates a StaffRequestsHandler
func NewStaffRequestHandler(logger *logrus.Entry, accountClient accountPb.AccountServiceClient, selfClient staffPb.StaffServiceClient) *StaffRequestsHandler {
	return &StaffRequestsHandler{
		Logger:        logger,
		AccountClient: accountClient,
		SelfClient:    selfClient,
	}
}

// RegisterStaffRoutes registers the routes to a mux router
func (s *StaffRequestsHandler) RegisterStaffRoutes(r *mux.Router) {
	r.HandleFunc("/v1/staff/{staff_uuid}/favourite/{account_uuid}", s.HandleCreateStaffFavouriteRequest).Methods("POST")
	r.HandleFunc("/v1/staff/{staff_uuid}/favourite/{account_uuid}", s.HandleDeleteStaffFavouriteRequest).Methods("DELETE")
	r.HandleFunc("/v1/staff/favourite/{account_uuid}", s.HandleGetStaffFavouriteForAccountRequest).Methods("GET")
}

// HandleCreateStaffFavouriteRequest handles a request for creating a staff favourite
func (s *StaffRequestsHandler) HandleCreateStaffFavouriteRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	staffUUID := vars["staff_uuid"]
	accountUUID := vars["account_uuid"]

	logger.Infof("HandleCreateStaffFavouriteRequest account '%s', staff '%s'", accountUUID, staffUUID)
	defer timeTrack(logger, time.Now(), "HandleCreateStaffFavouriteRequest")

	ctx := context.Background()

	account, err := s.AccountClient.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	response, err := s.SelfClient.GetStaff(ctx, &staffPb.GetStaffRequest{Uuids: staffUUID})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}
	if len(response.Staff) == 0 {
		res.WriteHeader(http.StatusNotFound)
		return
	}
	staff := response.Staff[0]

	_, err = s.SelfClient.CreateStaffFavourite(ctx, &staffPb.StaffFavouriteRequest{AccountId: account.Id, StaffId: staff.Id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	res.WriteHeader(http.StatusNoContent)
}

// HandleDeleteStaffFavouriteRequest handles a request for removing a staff favourite
func (s *StaffRequestsHandler) HandleDeleteStaffFavouriteRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	staffUUID := vars["staff_uuid"]
	accountUUID := vars["account_uuid"]

	logger.Infof("HandleDeleteStaffFavouriteRequest account '%s', staff '%s'", accountUUID, staffUUID)
	defer timeTrack(logger, time.Now(), "HandleDeleteStaffFavouriteRequest")

	ctx := context.Background()

	account, err := s.AccountClient.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	response, err := s.SelfClient.GetStaff(ctx, &staffPb.GetStaffRequest{Uuids: staffUUID})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}
	if len(response.Staff) == 0 {
		res.WriteHeader(http.StatusNotFound)
		return
	}
	staff := response.Staff[0]

	_, err = s.SelfClient.DeleteStaffFavourite(ctx, &staffPb.StaffFavouriteRequest{AccountId: account.Id, StaffId: staff.Id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	res.WriteHeader(http.StatusNoContent)
}

// HandleGetStaffFavouriteForAccountRequest fetches the favourite staff UUIDs for an acocunt
func (s *StaffRequestsHandler) HandleGetStaffFavouriteForAccountRequest(res http.ResponseWriter, req *http.Request) {
	logger := GetRequestLogger(req, s.Logger)

	vars := mux.Vars(req)
	accountUUID := vars["account_uuid"]

	logger.Infof("HandleGetStaffFavouriteForAccountRequest account '%s'", accountUUID)
	defer timeTrack(logger, time.Now(), "HandleGetStaffFavouriteForAccountRequest")

	ctx := context.Background()

	account, err := s.AccountClient.GetAccount(ctx, &accountPb.GetAccountRequest{Search: accountUUID})
	if err != nil {
		if grpc.Code(err) == codes.NotFound {
			res.WriteHeader(http.StatusNotFound)
			return
		}
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}

	response, err := s.SelfClient.GetStaffFavouriteForAccount(ctx, &staffPb.GetStaffFavouriteForAccountRequest{AccountId: account.Id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		s.writeJSONResponse(res, err)
		return
	}
	if response.StaffUuids == nil {
		response.StaffUuids = []string{}
	}

	res.WriteHeader(http.StatusOK)
	s.writeJSONResponse(res, response.StaffUuids)
}

func (s *StaffRequestsHandler) writeJSONResponse(res http.ResponseWriter, response interface{}) error {
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		s.Logger.Error("Failed to Marshal the response.")
		res.WriteHeader(http.StatusInternalServerError)
		return err
	}

	res.Write([]byte(jsonResponse))
	return nil
}
