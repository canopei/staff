package main

import (
	"context"
	"encoding/json"
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"google.golang.org/grpc"

	"bitbucket.org/canopei/golibs/healthcheck"
	"bitbucket.org/canopei/staff"
	"bitbucket.org/canopei/staff/config"

	"fmt"

	"bitbucket.org/canopei/account"
	cApi "bitbucket.org/canopei/golibs/api"
	cGrpc "bitbucket.org/canopei/golibs/grpc/utils"
	cHttp "bitbucket.org/canopei/golibs/http"
	"bitbucket.org/canopei/golibs/logging"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/urfave/negroni"
)

var (
	conf         *config.Config
	logger       *logrus.Entry
	staffHandler *StaffRequestsHandler
	selfClient   staffPb.StaffServiceClient
	version      string
)

func run() error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := mux.NewRouter()
	mux.HandleFunc(healthcheck.Healthpath, HealthcheckHandler)

	staffHandler.RegisterStaffRoutes(mux)

	gwmux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONBuiltin{}))
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBackoffMaxDelay(8 * time.Second)}

	err := staffPb.RegisterStaffServiceHandlerFromEndpoint(ctx, gwmux, fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort), opts)
	if err != nil {
		return err
	}
	mux.PathPrefix("/").Handler(cGrpc.RestGatewayResponseInterceptor(gwmux))

	n := negroni.New()
	n.Use(cHttp.NewXRequestIDMiddleware(16))
	n.Use(cApi.NewServiceAPIMiddleware(logger))
	n.Use(cApi.NewCombinedLoggingMiddleware(logger, mux, handlers.CombinedLoggingHandler))
	n.Use(negroni.NewRecovery())

	logger.Infof("Serving on %d.", conf.Service.ApiPort)
	return http.ListenAndServe(fmt.Sprintf(":%d", conf.Service.ApiPort), n)
}

func main() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("STAFF_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = config.LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}

	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{
		"subservice": "api",
	})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	selfClient, _, err = staff.NewClient(fmt.Sprintf("127.0.0.1:%d", conf.Service.GrpcPort))
	if err != nil {
		logger.Fatalf("Cannot connect to the self service: %v", err)
	}
	accountClient, _, err := account.NewAccountClient(conf.AccountService.Addr)
	if err != nil {
		logger.Fatalf("Cannot connect to the account service: %v", err)
	}
	staffHandler = NewStaffRequestHandler(logger, accountClient, selfClient)

	logger.Infof("Booting '%s' API (%s)...", conf.Service.Name, version)

	if err = run(); err != nil {
		logger.Fatal(err)
	}
}

// HealthcheckHandler handle the healthcheck request
func HealthcheckHandler(res http.ResponseWriter, req *http.Request) {
	localLogger := GetRequestLogger(req, logger)

	_, err := selfClient.Ping(context.Background(), &empty.Empty{})
	if err != nil {
		localLogger.Errorf("Ping error: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")
	msg, _ := json.Marshal(map[string]string{"status": "ok", "version": version})
	res.Write([]byte(msg))

	return
}
