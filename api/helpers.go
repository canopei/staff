package main

import (
	"net/http"
	"time"

	"github.com/Sirupsen/logrus"
)

// GetRequestLogger builds an instance of a logger for the current request using an original logger
// It adds a requestid Field to the logger
func GetRequestLogger(req *http.Request, originalLogger *logrus.Entry) *logrus.Entry {
	log := logrus.New()
	log.Formatter = originalLogger.Logger.Formatter
	log.Level = originalLogger.Logger.Level
	log.Hooks = originalLogger.Logger.Hooks
	newLogger := log.WithFields(originalLogger.Data)

	requestID := req.Header.Get("X-Request-Id")
	if requestID != "" {
		newLogger.Data["requestid"] = requestID
	}

	return newLogger
}

func timeTrack(logger *logrus.Entry, start time.Time, name string) {
	elapsed := time.Since(start)
	logger.Debugf("%s took %s", name, elapsed)
}
