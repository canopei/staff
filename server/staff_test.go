package main

import (
	"context"
	"testing"

	"fmt"

	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	staffColumns = []string{"staff_id", "uuid", "mb_id", "site_id", "name", "fname", "lname", "bio", "gender", "image_url",
		"email", "slug", "multi_location", "appointment_trn", "reservation_trn", "independent_contractor", "always_allow_double_booking",
		"user_access_level", "featured", "quote", "created_at", "modified_at", "deleted_at"}
	addressColumns       = []string{"staff_address_id", "staff_id", "address", "address2", "city", "state", "country", "postal_code", "foreign_zip"}
	phoneColumns         = []string{"staff_phone_id", "staff_id", "staff_phone_type_id", "number"}
	socialProfileColumns = []string{"staff_social_profile_id", "staff_id", "type", "url", "created_at", "modified_at", "deleted_at"}

	staffWithAddressColumns = []string{"staff_id", "uuid", "mb_id", "site_id", "name", "fname", "lname", "bio", "gender", "image_url",
		"email", "slug", "multi_location", "appointment_trn", "reservation_trn", "independent_contractor", "always_allow_double_booking",
		"user_access_level", "featured", "quote", "created_at", "modified_at", "deleted_at",
		"staff_address.address", "staff_address.address2", "staff_address.city", "staff_address.state", "staff_address.country",
		"staff_address.postal_code", "staff_address.foreign_zip",
	}

	favouriteColumns = []string{"staff_favourite_id", "staff_id", "account_id", "created_at"}
)

func TestCreateStaffValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.Staff
		ExpectedCode codes.Code
	}{
		{&staffPb.Staff{}, codes.InvalidArgument},
		{&staffPb.Staff{SiteId: 1, Address: &staffPb.StaffAddress{}}, codes.InvalidArgument},
		{&staffPb.Staff{SiteId: 1, Address: &staffPb.StaffAddress{}, Name: " "}, codes.InvalidArgument},
		{&staffPb.Staff{SiteId: 1, Name: "bar"}, codes.InvalidArgument},
		{&staffPb.Staff{Address: &staffPb.StaffAddress{}, Name: "foo"}, codes.InvalidArgument},
		{&staffPb.Staff{Address: &staffPb.StaffAddress{}, Name: "foo", Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{TypeName: "invalid", Number: "foo"},
		}}, codes.InvalidArgument},

		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.Staff{SiteId: 1, Name: "foo", Address: &staffPb.StaffAddress{}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		mock.ExpectBegin()
		staff, err := server.CreateStaff(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(staff, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateStaffSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testStaff := &staffPb.Staff{
		SiteId:   1,
		MbId:     123,
		Name:     "name",
		Gender:   1,
		Email:    "foo@bar.com",
		Featured: 20,
		Quote:    "lorem ipsum",
		Address: &staffPb.StaffAddress{
			Address2: "address2",
		},
		Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{
				TypeName: "work",
				Number:   "12345",
			},
			&staffPb.StaffPhone{
				TypeName: "mobile",
				Number:   "6789",
			},
		},
	}

	// slug unique check
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows([]string{"staff_id"}))

	mock.ExpectBegin()
	mock.ExpectExec("^INSERT INTO staff ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_details ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows(staffColumns).
		AddRow(1, "123456789012345", 123, 1, "name", "fname", "lname", "bio", 2, "image_url", "foo@bar.com", "foo-bar", false, false, false,
			false, false, "ual", 0, testStaff.Quote, "2017-01-01", "2017-01-02", "2017-01-03"))
	mock.ExpectExec("^INSERT INTO staff_address ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_phone ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_phone ").WillReturnResult(sqlmock.NewResult(2, 1))
	mock.ExpectCommit()

	staff, err := server.CreateStaff(context.Background(), testStaff)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(staff)
	if staff != nil {
		assert.NotEmpty(staff.Uuid)
		assert.Equal("name", staff.Name)
		assert.Equal(testStaff.Quote, staff.Quote)
	}
}

func TestCreateStaffSuccessfulWithSlugRetries(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testStaff := &staffPb.Staff{
		SiteId:   1,
		MbId:     123,
		Name:     "name",
		Gender:   1,
		Email:    "foo@bar.com",
		Featured: 20,
		Quote:    "lorem ipsum",
		Address: &staffPb.StaffAddress{
			Address2: "address2",
		},
		Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{
				TypeName: "work",
				Number:   "12345",
			},
			&staffPb.StaffPhone{
				TypeName: "mobile",
				Number:   "6789",
			},
		},
	}

	// slug unique check with 2 retries
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows([]string{"staff_id"}).AddRow(1))
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows([]string{"staff_id"}).AddRow(2))
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows([]string{"staff_id"}))

	mock.ExpectBegin()
	mock.ExpectExec("^INSERT INTO staff ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_details ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows(staffColumns).
		AddRow(1, "123456789012345", 123, 1, "name", "fname", "lname", "bio", 2, "image_url", "foo@bar.com", "foo-bar", false, false, false,
			false, false, "ual", 0, testStaff.Quote, "2017-01-01", "2017-01-02", "2017-01-03"))
	mock.ExpectExec("^INSERT INTO staff_address ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_phone ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_phone ").WillReturnResult(sqlmock.NewResult(2, 1))
	mock.ExpectCommit()

	staff, err := server.CreateStaff(context.Background(), testStaff)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(staff)
	if staff != nil {
		assert.NotEmpty(staff.Uuid)
		assert.Equal("name", staff.Name)
		assert.Equal(testStaff.Quote, staff.Quote)
	}
}

func TestCreateStaffRollback(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testStaff := &staffPb.Staff{
		SiteId:   1,
		MbId:     123,
		Name:     "name",
		Gender:   1,
		Email:    "foo@bar.com",
		Featured: 20,
		Quote:    "lorem ipsum",
		Address: &staffPb.StaffAddress{
			Address2: "address2",
		},
		Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{
				TypeName: "work",
				Number:   "12345",
			},
			&staffPb.StaffPhone{
				TypeName: "mobile",
				Number:   "6789",
			},
		},
	}

	// slug unique check
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows([]string{"staff_id"}))

	mock.ExpectBegin()
	mock.ExpectExec("^INSERT INTO staff ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_details ").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WillReturnRows(sqlmock.NewRows(staffColumns).
		AddRow(1, "123456789012345", 123, 1, "name", "fname", "lname", "bio", 2, "image_url", "foo@bar.com", "foo-bar", false, false, false,
			false, false, "ual", 0, "", "2017-01-01", "2017-01-02", "2017-01-03"))
	mock.ExpectExec("^INSERT INTO staff_address ").WillReturnError(fmt.Errorf("Test error"))
	mock.ExpectRollback()

	staff, err := server.CreateStaff(context.Background(), testStaff)

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(staff)
	assert.NotNil(err)
	assert.Equal(codes.Unknown, grpc.Code(err))
}

func TestGetStaffValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.GetStaffRequest
		ExpectedCode codes.Code
	}{
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.GetStaffRequest{}, codes.Unknown},
		{&staffPb.GetStaffRequest{Uuids: " "}, codes.Unknown},
		{&staffPb.GetStaffRequest{Uuids: " ,"}, codes.Unknown},
		{&staffPb.GetStaffRequest{Ids: []int32{1, 2}}, codes.Unknown},
		{&staffPb.GetStaffRequest{MbIds: []int64{1, 2}, Offset: 10, Limit: 15}, codes.Unknown},
		{&staffPb.GetStaffRequest{Uuids: "uuid1,uuid2", Offset: 10, Limit: 15}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		staff, err := server.GetStaff(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(staff, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetStaffSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	staff := []*staffPb.Staff{
		&staffPb.Staff{
			Id: 1, SiteId: 1, Uuid: "1234567890123456", MbId: 123, Name: "name1", Gender: 1, Email: "foo1@bar.com", Slug: "name1", Featured: 1, Quote: "",
			Address: &staffPb.StaffAddress{Address2: "address21"},
			Phones:  []*staffPb.StaffPhone{&staffPb.StaffPhone{TypeName: "work", Number: "112345"}},
		},
		&staffPb.Staff{
			Id: 2, SiteId: 1, Uuid: "1234567890123457", MbId: 124, Name: "name2", Gender: 1, Email: "foo2@bar.com", Slug: "name2", Featured: 0, Quote: "foo",
			Address: &staffPb.StaffAddress{Address2: "address22"},
			Phones:  []*staffPb.StaffPhone{&staffPb.StaffPhone{TypeName: "work", Number: "212345"}},
		},
		&staffPb.Staff{
			Id: 3, SiteId: 2, Uuid: "1234567890123458", MbId: 125, Name: "name3", Gender: 1, Email: "foo3@bar.com", Slug: "name3", Featured: 2, Quote: "",
			Address: &staffPb.StaffAddress{Address2: "address23"},
			Phones:  []*staffPb.StaffPhone{&staffPb.StaffPhone{TypeName: "work", Number: "312345"}},
		},
	}

	staffRows := sqlmock.NewRows(staffWithAddressColumns)
	for _, oneStaff := range staff {
		staffRows.AddRow(oneStaff.Id, oneStaff.Uuid, oneStaff.MbId, oneStaff.SiteId, oneStaff.Name, "fname", "lname", "bio", oneStaff.Gender,
			"imageurl", oneStaff.Email, oneStaff.Slug, false, false, false, false, false, "ual", oneStaff.Featured, oneStaff.Quote, "2017-01-01", "2017-01-02", "2017-01-03",
			"address", oneStaff.Address.Address2, "city", "state", "country", "postal_code", "foreign_zip",
		)
	}
	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(staffRows)

	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(sqlmock.NewRows(staffPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(3, "1234567890123457", 2, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_social_profile").WillReturnRows(sqlmock.NewRows(socialProfileColumns).
		AddRow(1, 1, "facebook", "foo", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, 1, "instagram", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(3, 3, "facebook", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_phone").WillReturnRows(sqlmock.NewRows([]string{"staff_id", "staff_phone_id", "type_name", "number"}).
		AddRow(1, 1, "work", "12345").
		AddRow(1, 2, "mobile", "67890").
		AddRow(2, 2, "mobile", "67890").
		AddRow(3, 2, "work", "67890"))

	response, err := server.GetStaff(context.Background(), &staffPb.GetStaffRequest{Uuids: "uuid1,uuid2", Offset: 10, Limit: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Staff, len(staff))

		assert.Len(response.Staff[0].Photos, 2)
		assert.Len(response.Staff[2].Photos, 0)

		assert.Len(response.Staff[0].SocialProfiles, 2)
		assert.Len(response.Staff[2].SocialProfiles, 1)
		assert.Equal("facebook", response.Staff[0].SocialProfiles[0].Type)

		assert.Len(response.Staff[0].Phones, 2)
		assert.Len(response.Staff[2].Phones, 1)
	}
}

func TestGetStaffSuccessfulSingle(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	staff := []*staffPb.Staff{
		&staffPb.Staff{
			Id: 2, SiteId: 1, Uuid: "1234567890123457", MbId: 124, Name: "name2", Gender: 1, Email: "foo2@bar.com", Slug: "slug2", Featured: 0, Quote: "foo",
			Address: &staffPb.StaffAddress{Address2: "address22"},
			Phones:  []*staffPb.StaffPhone{&staffPb.StaffPhone{TypeName: "work", Number: "212345"}},
		},
	}

	staffRows := sqlmock.NewRows(staffWithAddressColumns)
	for _, oneStaff := range staff {
		staffRows.AddRow(oneStaff.Id, oneStaff.Uuid, oneStaff.MbId, oneStaff.SiteId, oneStaff.Name, "fname", "lname", "bio", oneStaff.Gender,
			"imageurl", oneStaff.Email, oneStaff.Slug, false, false, false, false, false, "ual", oneStaff.Featured, oneStaff.Quote, "2017-01-01", "2017-01-02", "2017-01-03",
			"address", oneStaff.Address.Address2, "city", "state", "country", "postal_code", "foreign_zip",
		)
	}
	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(staffRows)

	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(sqlmock.NewRows(staffPhotoColumns).
		AddRow(1, "1234567890123456", 2, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 2, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(3, "1234567890123457", 2, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_social_profile").WillReturnRows(sqlmock.NewRows(socialProfileColumns).
		AddRow(1, 2, "facebook", "foo", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, 2, "instagram", "bar", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_phone").WillReturnRows(sqlmock.NewRows([]string{"staff_id", "staff_phone_id", "type_name", "number"}))

	response, err := server.GetStaff(context.Background(), &staffPb.GetStaffRequest{Uuids: "uuid1", Offset: 10, Limit: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Staff, len(staff))

		assert.Len(response.Staff[0].Photos, 3)

		assert.Len(response.Staff[0].SocialProfiles, 2)
		assert.Equal("instagram", response.Staff[0].SocialProfiles[1].Type)

		assert.Len(response.Staff[0].Phones, 0)
	}
}

func TestGetStaffEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	staffRows := sqlmock.NewRows(staffWithAddressColumns)
	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(staffRows)

	response, err := server.GetStaff(context.Background(), &staffPb.GetStaffRequest{Uuids: "uuid1,uuid2", Offset: 10, Limit: 15})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Staff, 0)
	}
}

func TestGetStaffBySlugValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.GetBySlugRequest
		ExpectedCode codes.Code
	}{
		{&staffPb.GetBySlugRequest{}, codes.InvalidArgument},
		{&staffPb.GetBySlugRequest{Slug: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.GetBySlugRequest{Slug: "foo-bar"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		staff, err := server.GetStaffBySlug(context.Background(), validationTest.Request)

		// we will always get nil
		assert.Nil(staff, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}
func TestGetStaffBySlugSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	oneStaff := &staffPb.Staff{
		Id: 1, SiteId: 1, Uuid: "1234567890123456", MbId: 123, Name: "name1", Gender: 1, Email: "foo1@bar.com", Slug: "name1", Featured: 1, Quote: "",
		Address: &staffPb.StaffAddress{Address2: "address21"},
		Phones:  []*staffPb.StaffPhone{&staffPb.StaffPhone{TypeName: "work", Number: "112345"}},
	}

	staffRows := sqlmock.NewRows(staffWithAddressColumns)
	staffRows.AddRow(oneStaff.Id, oneStaff.Uuid, oneStaff.MbId, oneStaff.SiteId, oneStaff.Name, "fname", "lname", "bio", oneStaff.Gender,
		"imageurl", oneStaff.Email, oneStaff.Slug, false, false, false, false, false, "ual", oneStaff.Featured, oneStaff.Quote, "2017-01-01", "2017-01-02", "2017-01-03",
		"address", oneStaff.Address.Address2, "city", "state", "country", "postal_code", "foreign_zip",
	)

	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(staffRows)

	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(sqlmock.NewRows(staffPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_social_profile").WillReturnRows(sqlmock.NewRows(socialProfileColumns).
		AddRow(1, 1, "facebook", "foo", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, 1, "instagram", "bar", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_phone").WillReturnRows(sqlmock.NewRows([]string{"staff_id", "staff_phone_id", "type_name", "number"}).
		AddRow(1, 1, "work", "12345").
		AddRow(1, 1, "mobile", "67890"))

	staff, err := server.GetStaffBySlug(context.Background(), &staffPb.GetBySlugRequest{Slug: "foo-bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(staff)
	if staff != nil {
		assert.Equal(oneStaff.Name, staff.Name)
		assert.Len(staff.Photos, 2)
		assert.Len(staff.SocialProfiles, 2)
		assert.Len(staff.Phones, 2)
	}
}

func TestGetStaffBySlugMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(sqlmock.NewRows(staffWithAddressColumns))

	staff, err := server.GetStaffBySlug(context.Background(), &staffPb.GetBySlugRequest{Slug: "foo-bar"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Nil(staff)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestDeleteStaffValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.DeleteStaffRequest
		ExpectedCode codes.Code
	}{
		{&staffPb.DeleteStaffRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.DeleteStaffRequest{Uuid: "foo"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteStaff(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteStaff(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE staff SET(.+)deleted_at").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteStaff(context.Background(), &staffPb.DeleteStaffRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE staff SET(.+)deleted_at").WithArgs("foo").WillReturnResult(sqlmock.NewResult(0, 1))

	_, err = server.DeleteStaff(context.Background(), &staffPb.DeleteStaffRequest{Uuid: "foo"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}

func TestGetStaffBySiteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.GetStaffBySiteRequest
		ExpectedCode codes.Code
	}{
		{&staffPb.GetStaffBySiteRequest{}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.GetStaffBySiteRequest{SiteId: 1}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		staff, err := server.GetStaffBySite(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(staff, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetStaffBySiteSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(sqlmock.NewRows(staffWithAddressColumns).
		AddRow(1, "1234567890123456", 123, 1, "name1", "fname1", "lname1", "bio1", 2, "image_url", "foo1@bar.com", "slug1", false, false, false,
			false, false, "ual", 0, "", "2017-01-01", "2017-01-02", "2017-01-03",
			"address", "address12", "city1", "state", "country", "postal_code", "foreign_zip1").
		AddRow(2, "1234567890123457", 124, 1, "name2", "fname2", "lname2", "bio2", 2, "image_url", "foo2@bar.com", "slug2", false, true, false,
			false, true, "", 12, "foo", "2017-01-01", "2017-01-02", "2017-01-03",
			"address", "address22", "city2", "state", "country", "postal_code", "foreign_zip2"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(sqlmock.NewRows(staffPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03"))

	response, err := server.GetStaffBySite(context.Background(), &staffPb.GetStaffBySiteRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Staff, 2)
		// sanity checks
		assert.Equal("foo2@bar.com", response.Staff[1].Email)
		assert.NotNil(response.Staff[0].Address)
		assert.Equal("city1", response.Staff[0].Address.City)
		assert.Len(response.Staff[0].Photos, 2)
		assert.Len(response.Staff[1].Photos, 0)
	}
}

func TestGetStaffBySiteEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(sqlmock.NewRows(staffWithAddressColumns))

	response, err := server.GetStaffBySite(context.Background(), &staffPb.GetStaffBySiteRequest{SiteId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Staff, 0)
	}
}

func TestUpdateStaffValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.Staff
		ExpectedCode codes.Code
	}{
		{&staffPb.Staff{}, codes.InvalidArgument},
		{&staffPb.Staff{Uuid: "foo", Address: &staffPb.StaffAddress{}}, codes.InvalidArgument},
		{&staffPb.Staff{Uuid: "foo", Address: &staffPb.StaffAddress{}, Name: " "}, codes.InvalidArgument},
		{&staffPb.Staff{Uuid: "foo", Name: "bar"}, codes.InvalidArgument},
		{&staffPb.Staff{Address: &staffPb.StaffAddress{}, Name: "bar"}, codes.InvalidArgument},
		{&staffPb.Staff{Uuid: "foo", Name: "bar", Address: &staffPb.StaffAddress{}, Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{TypeName: "invalid", Number: "foo"},
		}}, codes.InvalidArgument},
		{&staffPb.Staff{Address: &staffPb.StaffAddress{}, Name: "bar", SocialProfiles: []*staffPb.SocialProfile{
			&staffPb.SocialProfile{Type: "invalid", Url: "foo"},
		}}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.Staff{Uuid: "foo", Name: "bar", Address: &staffPb.StaffAddress{}}, codes.Unknown},
		{&staffPb.Staff{Uuid: "foo", Name: "bar", Address: &staffPb.StaffAddress{}, SocialProfiles: []*staffPb.SocialProfile{
			&staffPb.SocialProfile{Type: SocialProfileTypes[0], Url: "foo"},
		}}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		mock.ExpectBegin()
		staff, err := server.UpdateStaff(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(staff, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateStaffMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE staff ").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM staff ").WithArgs("foo").WillReturnRows(sqlmock.NewRows(staffWithAddressColumns))
	mock.ExpectRollback()

	staff, err := server.UpdateStaff(context.Background(), &staffPb.Staff{Uuid: "foo", Name: "bar", Address: &staffPb.StaffAddress{}})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(staff)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateStaffSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testStaff := &staffPb.Staff{
		Uuid:   "123456789012345",
		MbId:   123,
		SiteId: 1,
		Name:   "name",
		Address: &staffPb.StaffAddress{
			Address2: "addressnew",
		},
		Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{TypeName: "mobile", Number: "12345"},
			&staffPb.StaffPhone{TypeName: "home", Number: "123456"},
		},
		SocialProfiles: []*staffPb.SocialProfile{
			&staffPb.SocialProfile{Type: "facebook", Url: "foo"},
			&staffPb.SocialProfile{Type: "instagram", Url: "bar"},
		},
	}

	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE staff ").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(staffWithAddressColumns).
		AddRow(1, "1234567890123456", 123, 1, "name1", "fname1", "lname1", "bio1", 2, "image_url", "foo1@bar.com", "slug1", false, false, false,
			false, false, "ual", 2, "foo", "2017-01-01", "2017-01-02", "2017-01-03",
			"address", "addressold", "city1", "state", "country", "postal_code", "foreign_zip1")
	mock.ExpectQuery("^SELECT (.+) FROM staff").WithArgs(testStaff.Uuid).WillReturnRows(rows)
	mock.ExpectExec("^UPDATE staff_address SET").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("^DELETE FROM staff_phone").WillReturnResult(sqlmock.NewResult(0, 2))
	mock.ExpectExec("^INSERT INTO staff_phone").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_phone").WillReturnResult(sqlmock.NewResult(2, 1))

	mock.ExpectQuery("^SELECT (.+) FROM staff_social_profile").WillReturnRows(sqlmock.NewRows(socialProfileColumns).
		AddRow(2, 1, "instagram", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, 1, "facebook", "bar", "2017-01-01", "2017-01-02", "2017-01-03"))
	// Remove one profile
	mock.ExpectExec("^UPDATE staff_social_profile SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	// Add the new profile
	mock.ExpectExec("^INSERT INTO staff_social_profile").WillReturnResult(sqlmock.NewResult(0, 1))

	mock.ExpectCommit()

	staff, err := server.UpdateStaff(context.Background(), testStaff)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(staff)
	// sanity check
	if staff != nil {
		assert.Equal("name1", staff.Name)
		assert.Equal(int32(2), staff.Featured)
		assert.Equal("addressnew", staff.Address.Address2)
		assert.Len(staff.SocialProfiles, len(testStaff.SocialProfiles))
		assert.Len(staff.Phones, 2)
	}
}

// I should still get the social profiles if we are sending null in request.
func TestUpdateStaffNoSocialProfilesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testStaff := &staffPb.Staff{
		Uuid:   "123456789012345",
		MbId:   123,
		SiteId: 1,
		Name:   "name",
		Address: &staffPb.StaffAddress{
			Address2: "addressnew",
		},
		Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{TypeName: "mobile", Number: "12345"},
			&staffPb.StaffPhone{TypeName: "home", Number: "123456"},
		},
	}

	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE staff ").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(staffWithAddressColumns).
		AddRow(1, "1234567890123456", 123, 1, "name1", "fname1", "lname1", "bio1", 2, "image_url", "foo1@bar.com", "slug", false, false, false,
			false, false, "ual", 2, "foo", "2017-01-01", "2017-01-02", "2017-01-03",
			"address", "addressold", "city1", "state", "country", "postal_code", "foreign_zip1")
	mock.ExpectQuery("^SELECT (.+) FROM staff").WithArgs(testStaff.Uuid).WillReturnRows(rows)
	mock.ExpectExec("^UPDATE staff_address SET").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("^DELETE FROM staff_phone").WillReturnResult(sqlmock.NewResult(0, 2))
	mock.ExpectExec("^INSERT INTO staff_phone").WillReturnResult(sqlmock.NewResult(1, 1))
	mock.ExpectExec("^INSERT INTO staff_phone").WillReturnResult(sqlmock.NewResult(2, 1))

	mock.ExpectQuery("^SELECT (.+) FROM staff_social_profile").WillReturnRows(sqlmock.NewRows(socialProfileColumns).
		AddRow(2, 1, "instagram", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, 1, "facebook", "bar", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectCommit()

	staff, err := server.UpdateStaff(context.Background(), testStaff)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(staff)
	// sanity check
	if staff != nil {
		assert.Equal("name1", staff.Name)
		assert.Equal(int32(2), staff.Featured)
		assert.Equal("addressnew", staff.Address.Address2)
		assert.Len(staff.SocialProfiles, 2)
		assert.Len(staff.Phones, 2)
	}
}

// I should still get the phone numbers if we are sending null in request.
func TestUpdateStaffNoPhonesSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testStaff := &staffPb.Staff{
		Uuid:   "123456789012345",
		MbId:   123,
		SiteId: 1,
		Name:   "name",
		Address: &staffPb.StaffAddress{
			Address2: "addressnew",
		},
	}

	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE staff ").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(staffWithAddressColumns).
		AddRow(1, "1234567890123456", 123, 1, "name1", "fname1", "lname1", "bio1", 2, "image_url", "foo1@bar.com", "slug1", false, false, false,
			false, false, "ual", 2, "foo", "2017-01-01", "2017-01-02", "2017-01-03",
			"address", "addressold", "city1", "state", "country", "postal_code", "foreign_zip1")
	mock.ExpectQuery("^SELECT (.+) FROM staff").WithArgs(testStaff.Uuid).WillReturnRows(rows)
	mock.ExpectExec("^UPDATE staff_address SET").WillReturnResult(sqlmock.NewResult(0, 1))

	mock.ExpectQuery("^SELECT (.+) FROM staff_phone").WillReturnRows(sqlmock.NewRows([]string{"staff_phone_id", "type_name", "number"}).
		AddRow(1, "work", "12345").AddRow(2, "mobile", "67890"))

	mock.ExpectQuery("^SELECT (.+) FROM staff_social_profile").WillReturnRows(sqlmock.NewRows(socialProfileColumns).
		AddRow(2, 1, "instagram", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, 1, "facebook", "bar", "2017-01-01", "2017-01-02", "2017-01-03"))

	mock.ExpectCommit()

	staff, err := server.UpdateStaff(context.Background(), testStaff)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(staff)
	// sanity check
	if staff != nil {
		assert.Equal("name1", staff.Name)
		assert.Equal(int32(2), staff.Featured)
		assert.Equal("addressnew", staff.Address.Address2)
		assert.Len(staff.SocialProfiles, 2)
		assert.Len(staff.Phones, 2)
	}
}

func TestUpdateStaffPhoneError(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testStaff := &staffPb.Staff{
		Uuid:   "123456789012345",
		MbId:   123,
		SiteId: 1,
		Name:   "name",
		Address: &staffPb.StaffAddress{
			Address2: "address2",
		},
		Phones: []*staffPb.StaffPhone{
			&staffPb.StaffPhone{TypeName: "mobile", Number: "12345"},
			&staffPb.StaffPhone{TypeName: "home", Number: "123456"},
		},
	}

	mock.ExpectBegin()
	mock.ExpectExec("^UPDATE staff ").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(staffWithAddressColumns).
		AddRow(1, "1234567890123456", 123, 1, "name1", "fname1", "lname1", "bio1", 2, "image_url", "foo1@bar.com", "slug1", false, false, false,
			false, false, "ual", 1, "foo", "2017-01-01", "2017-01-02", "2017-01-03",
			"address", "address12", "city1", "state", "country", "postal_code", "foreign_zip1")
	mock.ExpectQuery("^SELECT (.+) FROM staff").WithArgs(testStaff.Uuid).WillReturnRows(rows)
	mock.ExpectExec("^UPDATE staff_address SET").WillReturnResult(sqlmock.NewResult(0, 1))
	mock.ExpectExec("^DELETE FROM staff_phone").WillReturnResult(sqlmock.NewResult(0, 2))
	mock.ExpectExec("^INSERT INTO staff_phone").WillReturnError(fmt.Errorf("Test error"))
	mock.ExpectRollback()

	staff, err := server.UpdateStaff(context.Background(), testStaff)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(staff)
	assert.NotNil(err)
	assert.Equal(codes.Unknown, grpc.Code(err))
}

func TestCreateStaffFavouriteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.StaffFavouriteRequest
		ExpectedCode codes.Code
	}{
		{&staffPb.StaffFavouriteRequest{}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{AccountId: 1}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{StaffId: 2}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{AccountId: 0, StaffId: 2}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{AccountId: 1, StaffId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.StaffFavouriteRequest{AccountId: 1, StaffId: 2}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.CreateStaffFavourite(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateStaffFavouriteSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^INSERT IGNORE INTO staff_favourite ").WillReturnResult(sqlmock.NewResult(1, 1))

	response, err := server.CreateStaffFavourite(context.Background(), &staffPb.StaffFavouriteRequest{AccountId: 1, StaffId: 2})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
}

func TestDeleteStaffFavouriteValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.StaffFavouriteRequest
		ExpectedCode codes.Code
	}{
		{&staffPb.StaffFavouriteRequest{}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{AccountId: 1}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{StaffId: 2}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{AccountId: 0, StaffId: 2}, codes.InvalidArgument},
		{&staffPb.StaffFavouriteRequest{AccountId: 1, StaffId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.StaffFavouriteRequest{AccountId: 1, StaffId: 2}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.DeleteStaffFavourite(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteStaffFavouriteSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^DELETE FROM staff_favourite ").WillReturnResult(sqlmock.NewResult(0, 1))

	response, err := server.DeleteStaffFavourite(context.Background(), &staffPb.StaffFavouriteRequest{AccountId: 1, StaffId: 2})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
}

func TestGetStaffFavouriteForAccountValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.GetStaffFavouriteForAccountRequest
		ExpectedCode codes.Code
	}{
		{&staffPb.GetStaffFavouriteForAccountRequest{}, codes.InvalidArgument},
		{&staffPb.GetStaffFavouriteForAccountRequest{AccountId: 0}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.GetStaffFavouriteForAccountRequest{AccountId: 123}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		response, err := server.GetStaffFavouriteForAccount(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(response, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetStaffFavouriteForAccountSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows([]string{"staff_uuid"}).AddRow("1234567890123456").AddRow("1234567890123457")
	mock.ExpectQuery("^SELECT (.+) FROM staff_favourite ").WillReturnRows(rows)

	response, err := server.GetStaffFavouriteForAccount(context.Background(), &staffPb.GetStaffFavouriteForAccountRequest{AccountId: 1})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	assert.Len(response.StaffUuids, 2)
}
