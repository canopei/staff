package main

import (
	"database/sql"
	"strings"
	"time"

	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"github.com/golang/protobuf/ptypes/empty"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"

	staffPb "bitbucket.org/canopei/staff/protobuf"
)

// CreateStaffPhoto creates a new StaffPhoto
func (s *Server) CreateStaffPhoto(ctx context.Context, req *staffPb.StaffPhoto) (*staffPb.StaffPhoto, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateStaffPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "CreateStaffPhoto")

	// Validations
	req.Path = strings.TrimSpace(req.Path)
	if req.Path == "" {
		logger.Errorf("The photo file path is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo file path is required.")
	}
	req.StaffUuid = strings.TrimSpace(req.StaffUuid)
	if req.StaffUuid == "" {
		logger.Errorf("The staff UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff UUID is required.")
	}

	// Check the staff
	var staff staffPb.Staff
	err := s.DB.Get(&staff, `SELECT * FROM staff WHERE uuid = UNHEX(REPLACE(?,'-','')) AND deleted_at = '0'`, req.StaffUuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find staff '%s'.", req.StaffUuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching staff")
	}
	req.StaffId = staff.Id

	// Create a UUID
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	_, err = s.DB.NamedExec(`INSERT INTO staff_photo (uuid, staff_id, path, caption)
		VALUES (:uuid, :staff_id, :path, :caption)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	var photo = &staffPb.StaffPhoto{}
	err = s.DB.Get(photo, "SELECT * FROM staff_photo WHERE staff_photo_id = LAST_INSERT_ID()")
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, grpcUtils.InternalError(logger, err, "Cannot find the new staff photo.")
		}

		logger.Errorf("Error while fetching the new staff photo: %v", err)
		return nil, err
	}

	logger.Debugf("Created new staff photo %d.", photo.Id)

	return photo, nil
}

// GetStaffPhoto retrieves a StaffPhoto by UUID
func (s *Server) GetStaffPhoto(ctx context.Context, req *staffPb.StaffPhoto) (*staffPb.StaffPhoto, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetStaffPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "GetStaffPhoto")

	// Validations
	req.StaffUuid = strings.TrimSpace(req.StaffUuid)
	if req.StaffUuid == "" {
		logger.Errorf("The staff UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff UUID is required.")
	}
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Errorf("The photo UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo UUID is required.")
	}

	var photo staffPb.StaffPhoto
	err := s.DB.Get(&photo, `SELECT sp.*
				FROM staff_photo sp
				INNER JOIN staff s ON s.staff_id = sp.staff_id
				WHERE s.uuid = UNHEX(REPLACE(?,'-',''))
					AND sp.uuid = UNHEX(REPLACE(?,'-',''))
					AND s.deleted_at = '0'
					AND sp.deleted_at = '0'`, req.StaffUuid, req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find staff photo UUID '%s' for staff '%s'.", req.Uuid, req.StaffUuid)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff photo")
	}
	photo.Uuid = req.Uuid

	return &photo, nil
}

// GetStaffPhotos retrieves the StaffPhotos of a Staff
func (s *Server) GetStaffPhotos(ctx context.Context, req *staffPb.StaffPhoto) (*staffPb.StaffPhotoList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetStaffPhotos: %v", req)
	defer timeTrack(logger, time.Now(), "GetStaffPhotos")

	// Validations
	req.StaffUuid = strings.TrimSpace(req.StaffUuid)
	if req.StaffUuid == "" {
		logger.Errorf("The staff UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff UUID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT sp.*
		FROM staff_photo sp
		INNER JOIN staff s ON s.staff_id = sp.staff_id
		WHERE s.uuid = UNHEX(REPLACE(?,'-','')) AND s.deleted_at = '0' AND sp.deleted_at = '0'`, req.StaffUuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff photos")
	}

	staffPhotosList := &staffPb.StaffPhotoList{}
	for rows.Next() {
		photo := &staffPb.StaffPhoto{}
		err := rows.StructScan(photo)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning a staff photo")
		}

		unpackedUUID, err := crypto.Parse([]byte(photo.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		photo.Uuid = unpackedUUID.String()

		staffPhotosList.Photos = append(staffPhotosList.Photos, photo)
	}

	return staffPhotosList, nil
}

// UpdateStaffPhoto updates a StaffPhoto
func (s *Server) UpdateStaffPhoto(ctx context.Context, req *staffPb.StaffPhoto) (*staffPb.StaffPhoto, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateStaffPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateStaffPhoto")

	// Validations
	req.StaffUuid = strings.TrimSpace(req.StaffUuid)
	if req.StaffUuid == "" {
		logger.Errorf("The staff UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff UUID is required.")
	}
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Errorf("The photo UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo UUID is required.")
	}

	_, err := s.DB.NamedExec(`UPDATE staff_photo sp
		INNER JOIN staff s ON s.staff_id = sp.staff_id
		SET path = :path, caption = :caption
		WHERE s.uuid = UNHEX(REPLACE(:staff_uuid,'-',''))
			AND sp.uuid = UNHEX(REPLACE(:uuid,'-',''))
			AND s.deleted_at = '0'
			AND sp.deleted_at = '0'`, req)

	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - update.")
	}

	var photo staffPb.StaffPhoto
	err = s.DB.Get(&photo, `SELECT sp.*
		FROM staff_photo sp
		INNER JOIN staff s ON s.staff_id = sp.staff_id
		WHERE s.uuid = UNHEX(REPLACE(?,'-',''))
			AND sp.uuid = UNHEX(REPLACE(?,'-',''))
			AND s.deleted_at = '0'
			AND sp.deleted_at = '0'`, req.StaffUuid, req.Uuid)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("UpdateStaffPhoto - staff photo not found: %d", req.Id)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Unable to query database - select.")
	}
	photo.Uuid = req.Uuid

	return &photo, nil
}

// DeleteStaffPhoto removes a StaffPhoto (sets deleted_at)
func (s *Server) DeleteStaffPhoto(ctx context.Context, req *staffPb.StaffPhoto) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteStaffPhoto: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteStaffPhoto")

	// Validations
	req.StaffUuid = strings.TrimSpace(req.StaffUuid)
	if req.StaffUuid == "" {
		logger.Errorf("The staff UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff UUID is required.")
	}
	req.Uuid = strings.TrimSpace(req.Uuid)
	if req.Uuid == "" {
		logger.Errorf("The photo UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The photo UUID is required.")
	}

	result, err := s.DB.Exec(`UPDATE staff_photo sp
		INNER JOIN staff s ON s.staff_id = sp.staff_id
		SET sp.deleted_at = CURRENT_TIMESTAMP
		WHERE s.uuid = UNHEX(REPLACE(?,'-',''))
			AND sp.uuid = UNHEX(REPLACE(?,'-',''))
			AND s.deleted_at = '0'
			AND sp.deleted_at = '0'`, req.StaffUuid, req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteProgram - staff photo not found: %d", req.Id)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}
