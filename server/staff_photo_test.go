package main

import (
	"context"
	"testing"

	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/jmoiron/sqlx"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	sqlmock "gopkg.in/DATA-DOG/go-sqlmock.v1"
)

var (
	staffPhotoColumns = []string{"staff_photo_id", "uuid", "staff_id", "path", "caption", "created_at", "modified_at", "deleted_at"}
)

func TestCreateStaffPhotoValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.StaffPhoto
		ExpectedCode codes.Code
	}{
		{&staffPb.StaffPhoto{}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{Path: " "}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{Path: "foo"}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.StaffPhoto{Path: "foo", StaffUuid: "1234567890123456"}, codes.Unknown},
		{&staffPb.StaffPhoto{Path: "foo", StaffUuid: "1234567890123456", Caption: " "}, codes.Unknown},
		{&staffPb.StaffPhoto{Path: "foo", StaffUuid: "1234567890123456", Caption: "test caption"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.CreateStaffPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestCreateStaffPhotoSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM staff").WillReturnRows(sqlmock.NewRows(staffColumns).
		AddRow(1, "123456789012345", 123, 1, "name", "fname", "lname", "bio", 2, "image_url", "foo@bar.com", "slug", false, false, false,
			false, false, "ual", 0, "quote", "2017-01-01", "2017-01-02", "2017-01-03"))
	mock.ExpectExec("^INSERT INTO staff_photo").WillReturnResult(sqlmock.NewResult(1, 1))
	rows := sqlmock.NewRows(staffPhotoColumns).AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(rows)

	photo, err := server.CreateStaffPhoto(context.Background(), &staffPb.StaffPhoto{
		Path:      "foo",
		StaffUuid: "1234567890123456",
		Caption:   "bar",
	})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(photo)
	if photo != nil {
		assert.Equal("bar", photo.Caption)
	}
}

func TestGetStaffPhotosValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.StaffPhoto
		ExpectedCode codes.Code
	}{
		{&staffPb.StaffPhoto{}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{StaffUuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.StaffPhoto{StaffUuid: "1234567890123456"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.GetStaffPhotos(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetStaffPhotosSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(staffPhotoColumns).
		AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03").
		AddRow(2, "1234567890123457", 1, "/some/path/to-photo_test2.jpeg", "foo", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(rows)

	response, err := server.GetStaffPhotos(context.Background(), &staffPb.StaffPhoto{StaffUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Photos, 2)
	}
}

func TestGetStaffPhotosEmpty(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(staffPhotoColumns)
	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(rows)

	response, err := server.GetStaffPhotos(context.Background(), &staffPb.StaffPhoto{StaffUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(response)
	if response != nil {
		assert.Len(response.Photos, 0)
	}
}

func TestGetStaffPhotoValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.StaffPhoto
		ExpectedCode codes.Code
	}{
		{&staffPb.StaffPhoto{}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{Uuid: "1234567890123456"}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{StaffUuid: "1234567890123456"}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.StaffPhoto{Uuid: "1234567890123456", StaffUuid: "1234567890123456"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.GetStaffPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestGetStaffPhotoSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	rows := sqlmock.NewRows(staffPhotoColumns).AddRow(1, "1234567890123456", 1, "/some/path/to-photo_test.jpeg", "bar", "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(rows)

	photo, err := server.GetStaffPhoto(context.Background(), &staffPb.StaffPhoto{Uuid: "1234567890123456", StaffUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(photo)
	if photo != nil {
		assert.Equal("bar", photo.Caption)
	}
}

func TestGetStaffPhotoMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(sqlmock.NewRows(staffPhotoColumns))

	photo, err := server.GetStaffPhoto(context.Background(), &staffPb.StaffPhoto{Uuid: "1234567890123456", StaffUuid: "1234567890123456"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(photo)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateProgramValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.StaffPhoto
		ExpectedCode codes.Code
	}{
		{&staffPb.StaffPhoto{}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{StaffUuid: "1234567890123456"}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{Uuid: "1234567890123457"}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{StaffUuid: " ", Uuid: "1234567890123457"}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{StaffUuid: "1234567890123456", Uuid: " "}, codes.InvalidArgument},

		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.StaffPhoto{Path: "foo", StaffUuid: "1234567890123456", Uuid: "1234567890123457"}, codes.Unknown},
		{&staffPb.StaffPhoto{Path: "foo", StaffUuid: "1234567890123456", Uuid: "1234567890123457", Caption: " "}, codes.Unknown},
		{&staffPb.StaffPhoto{Path: "foo", StaffUuid: "1234567890123456", Uuid: "1234567890123457", Caption: "test caption"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		photo, err := server.UpdateStaffPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(photo, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestUpdateStaffPhotoMissing(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	mock.ExpectExec("^UPDATE staff_photo").WillReturnResult(sqlmock.NewResult(0, 0))
	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(sqlmock.NewRows(staffPhotoColumns))

	photo, err := server.UpdateStaffPhoto(context.Background(), &staffPb.StaffPhoto{Path: "foo", StaffUuid: "1234567890123456", Uuid: "1234567890123457", Caption: "test caption"})

	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(photo)
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))
}

func TestUpdateStaffPhotoSuccessful(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	testPhoto := &staffPb.StaffPhoto{
		StaffUuid: "1234567890123457",
		Uuid:      "1234567890123456",
		Path:      "/test/path.jpeg",
		Caption:   "foo",
	}

	mock.ExpectExec("^UPDATE staff_photo(.+)SET").WillReturnResult(sqlmock.NewResult(0, 1))
	rows := sqlmock.NewRows(staffPhotoColumns).AddRow(1, testPhoto.Uuid, 1, testPhoto.Path, testPhoto.Caption, "2017-01-01", "2017-01-02", "2017-01-03")
	mock.ExpectQuery("^SELECT (.+) FROM staff_photo").WillReturnRows(rows)

	photo, err := server.UpdateStaffPhoto(context.Background(), testPhoto)
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
	assert.NotNil(photo)
	// sanity check
	if photo != nil {
		assert.Equal(testPhoto.Caption, photo.Caption)
	}
}

func TestDeleteStaffPhotoValidation(t *testing.T) {
	assert := assert.New(t)

	mockDB, _, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Validations tests
	validationTests := []struct {
		Request      *staffPb.StaffPhoto
		ExpectedCode codes.Code
	}{
		{&staffPb.StaffPhoto{}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{Uuid: "1234567890123456"}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{StaffUuid: "1234567890123456"}, codes.InvalidArgument},
		{&staffPb.StaffPhoto{Uuid: " "}, codes.InvalidArgument},
		// Here we will never get the roles, because we didn't mock this yet
		// but in this case we'll get an codes.Unknown error.
		{&staffPb.StaffPhoto{Uuid: "1234567890123456", StaffUuid: "1234567890123456"}, codes.Unknown},
	}
	for i, validationTest := range validationTests {
		result, err := server.DeleteStaffPhoto(context.Background(), validationTest.Request)
		// we will always get nil
		assert.Nil(result, "Failed for case %d.", i)
		assert.Equal(validationTest.ExpectedCode, grpc.Code(err), "Failed for case %d.", i)
	}
}

func TestDeleteStaffPhoto(t *testing.T) {
	assert := assert.New(t)

	mockDB, mock, err := sqlmock.New()
	assert.Nil(err, "an error '%s' was not expected when opening a stub database connection", err)
	defer mockDB.Close()
	sqlxDB := sqlx.NewDb(mockDB, "sqlmock")

	server := NewServerMock(sqlxDB)

	// Not found error
	mock.ExpectExec("^UPDATE staff_photo (.+)SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 0))

	_, err = server.DeleteStaffPhoto(context.Background(), &staffPb.StaffPhoto{Uuid: "1234567890123456", StaffUuid: "1234567890123456"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.NotNil(err)
	assert.Equal(codes.NotFound, grpc.Code(err))

	// Successful delete
	mock.ExpectExec("^UPDATE staff_photo (.+)SET(.+)deleted_at").WillReturnResult(sqlmock.NewResult(0, 1))
	_, err = server.DeleteStaffPhoto(context.Background(), &staffPb.StaffPhoto{Uuid: "1234567890123456", StaffUuid: "1234567890123456"})
	assert.Nil(mock.ExpectationsWereMet(), "there were unfulfilled expection")
	assert.Nil(err)
}
