package main

import (
	"io/ioutil"

	staffMb "bitbucket.org/canopei/mindbody/services/staff"
	"github.com/Sirupsen/logrus"
	"github.com/jmoiron/sqlx"
	nsq "github.com/nsqio/go-nsq"
)

// NewServerMock creates a mock of Server
func NewServerMock(db *sqlx.DB) *Server {
	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	staffMBClient := staffMb.NewStaff_x0020_ServiceSoap("", false, nil)

	queueProducer := &nsq.Producer{}

	return NewServer(logger, db, staffMBClient, queueProducer)
}
