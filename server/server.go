package main

import (
	"golang.org/x/net/context"

	staffMb "bitbucket.org/canopei/mindbody/services/staff"
	"bitbucket.org/canopei/staff"
	"github.com/Sirupsen/logrus"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	nsq "github.com/nsqio/go-nsq"
)

// Server is the account service implementation
type Server struct {
	Logger        *logrus.Entry
	DB            *sqlx.DB
	StaffMBClient *staffMb.Staff_x0020_ServiceSoap
	Queue         *nsq.Producer
	SyncService   *staff.SyncService
}

// NewServer creates a new Server instance
func NewServer(
	logger *logrus.Entry,
	db *sqlx.DB,
	staffMBClient *staffMb.Staff_x0020_ServiceSoap,
	queue *nsq.Producer,
) *Server {
	return &Server{
		Logger:        logger,
		DB:            db,
		StaffMBClient: staffMBClient,
		Queue:         queue,
	}
}

// Ping is used for healthchecks
func (s *Server) Ping(ctx context.Context, req *empty.Empty) (*empty.Empty, error) {
	return &empty.Empty{}, nil
}
