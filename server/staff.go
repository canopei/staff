package main

import (
	"database/sql"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"golang.org/x/net/context"

	"bitbucket.org/canopei/golibs/crypto"
	grpcUtils "bitbucket.org/canopei/golibs/grpc/utils"
	"bitbucket.org/canopei/golibs/slices"
	cStrings "bitbucket.org/canopei/golibs/strings"
	staffPb "bitbucket.org/canopei/staff/protobuf"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

const (
	// MaxStaffFetched is the limit of number of staff that can be fetched
	// at once.
	MaxStaffFetched int32 = 50
)

var (
	// PhoneType2DBID is a map of phone types to their DB IDs
	PhoneType2DBID = map[string]int{
		"mobile": 1,
		"work":   2,
		"home":   3,
	}

	// SocialProfileTypes is the list of valid social profile types
	SocialProfileTypes = []string{
		"facebook",
		"instagram",
		"twitter",
	}
)

// CreateStaff creates a Staff
func (s *Server) CreateStaff(ctx context.Context, req *staffPb.Staff) (*staffPb.Staff, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateStaff: %#v", req)
	defer timeTrack(logger, time.Now(), "CreateStaff")

	// Validations
	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Error("The staff name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff name is required.")
	}
	if req.Address == nil {
		logger.Error("The staff address is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff address is required.")
	}
	// Validate the phones
	if req.Phones != nil {
		for _, ph := range req.Phones {
			ph.TypeName = strings.ToLower(ph.TypeName)

			if _, ok := PhoneType2DBID[ph.TypeName]; !ok {
				logger.Errorf("Invalid phone type '%s'.", ph.TypeName)
				return nil, grpc.Errorf(codes.InvalidArgument, "Invalid phone type '%s'.", ph.TypeName)
			}
		}
	}

	// Create a UUID
	uuid, err := crypto.NewUUID()
	if err != nil {
		logger.Errorf("Cannot generate a UUID: %v", err)
		return nil, grpcUtils.InternalError(logger, err, "Cannot generate a UUID")
	}
	req.Uuid = uuid.String()

	// Generate unique slug
	rand.Seed(time.Now().Unix())
	existing := true
	for existing {
		req.Slug = fmt.Sprintf("%s-%d", cStrings.Slugify(req.Name), rand.Intn(999)+1000)

		rows, err := s.DB.Queryx(`SELECT staff_id FROM staff WHERE slug = ? AND deleted_at = '0'`, req.Slug)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while checking for existing staff by slug.")
		}
		if !rows.Next() {
			existing = false
		}
	}

	tx := s.DB.MustBegin()

	// Create the staff
	result, err := tx.NamedExec(`INSERT INTO staff
			(uuid, mb_id, site_id, name, fname, lname, bio, gender, image_url, email, slug, multi_location, appointment_trn,
			reservation_trn, independent_contractor, always_allow_double_booking, user_access_level, is_approved)
		VALUES (unhex(replace(:uuid,'-','')), :mb_id, :site_id, :name, :fname, :lname, :bio, :gender, :image_url, :email,
			:slug, :multi_location, :appointment_trn, :reservation_trn, :independent_contractor, :always_allow_double_booking,
			:user_access_level, :is_approved)`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}
	logger.Debugf("Created staff '%s'.", req.GetUuid())

	newStaffID, err := result.LastInsertId()
	if err != nil {
		tx.Rollback()
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the new staff ID")
	}

	// create the staff details entry
	_, err = tx.Exec(`INSERT INTO staff_details (staff_id, featured, quote) VALUES (?, 0, '')`, newStaffID)
	if err != nil {
		tx.Rollback()
		return nil, grpcUtils.InternalError(logger, err, "Cannot create staff_details database entry.")
	}

	// Get the staff
	newStaff := &staffPb.Staff{}
	err = tx.Get(newStaff, `SELECT
			s.*,
			sd.featured,
			sd.quote
		FROM staff s
		INNER JOIN staff_details sd ON sd.staff_id = s.staff_id
		WHERE uuid = unhex(replace(?,'-',''))`, req.Uuid)
	if err != nil {
		tx.Rollback()
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the new staff")
	}
	newStaff.Uuid = req.Uuid

	// create the address
	if req.Address != nil {
		req.Address.StaffId = newStaff.Id
		_, err = tx.NamedExec(`INSERT INTO staff_address
				(staff_id, address, address2, city, state, country, postal_code, foreign_zip)
			VALUES (:staff_id, :address, :address2, :city, :state, :country, :postal_code, :foreign_zip)`, req.Address)
		if err != nil {
			tx.Rollback()
			return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
		}
		logger.Debugf("Created address for '%s'.", req.GetUuid())
	}

	// add the phones
	if req.Phones != nil {
		for _, ph := range req.Phones {
			ph.StaffId = newStaff.Id
			ph.TypeName = strings.ToLower(ph.TypeName)

			_, err = tx.NamedExec(`INSERT INTO staff_phone
					(staff_id, staff_phone_type_id, number)
				VALUES (:staff_id, (SELECT staff_phone_type_id FROM staff_phone_type WHERE name = :type_name), :number)`, ph)
			if err != nil {
				tx.Rollback()
				return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
			}
		}
		logger.Debugf("Created phone(s) for '%s'.", req.GetUuid())
	}
	newStaff.Phones = req.Phones

	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		return nil, grpcUtils.InternalError(logger, err, "Failed to commit the DB transaction.")
	}

	return newStaff, nil
}

// GetStaff fetches a list of staff members
func (s *Server) GetStaff(ctx context.Context, req *staffPb.GetStaffRequest) (*staffPb.StaffList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetStaff: %v", req)
	defer timeTrack(logger, time.Now(), "GetStaff")

	if req.Limit == 0 || req.Limit > MaxStaffFetched {
		req.Limit = MaxStaffFetched
	}

	logger.Debugf("Fetching %d staff members, offset %d...", req.Limit, req.Offset)

	// Fetch the staff
	queryParams := map[string]interface{}{
		"limit":   req.Limit,
		"offset":  req.Offset,
		"site_id": req.SiteId,
	}

	req.Uuids = strings.TrimSpace(req.Uuids)
	reqUUIDs := map[string]bool{}
	if req.Uuids != "" {
		uuids := strings.Split(req.Uuids, ",")
		for _, uuid := range uuids {
			uuid := strings.TrimSpace(uuid)
			if uuid != "" {
				reqUUIDs[uuid] = true
			}
		}
	}

	condition := ""
	if len(reqUUIDs) > 0 {
		condition = "AND s.uuid IN ("
		idx := 0
		for v := range reqUUIDs {
			paramKey := "id" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%sUNHEX(REPLACE(:%s,'-','')), ", condition, paramKey)
			queryParams[paramKey] = v
			idx++
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	if len(req.MbIds) > 0 && req.SiteId > 0 {
		condition = "s.mb_id IN ("
		for idx, v := range req.MbIds {
			paramKey := "mbid" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
		condition = "AND (" + condition + " AND s.site_id = :site_id)"
	}

	if len(req.Ids) > 0 {
		condition = "AND s.staff_id IN ("
		for idx, v := range req.Ids {
			paramKey := "sid" + strconv.Itoa(idx)
			condition = fmt.Sprintf("%s:%s, ", condition, paramKey)
			queryParams[paramKey] = v
		}
		condition = strings.TrimSuffix(condition, ", ") + ") "
	}

	rows, err := s.DB.NamedQuery(`SELECT
			s.*,
			sd.featured,
			sd.quote,
			a.address "staff_address.address",
			a.address2 "staff_address.address2",
			a.city "staff_address.city",
			a.state "staff_address.state",
			a.country "staff_address.country",
			a.postal_code "staff_address.postal_code",
			a.foreign_zip "staff_address.foreign_zip"
		FROM staff s
		INNER JOIN staff_details sd ON s.staff_id = sd.staff_id
		LEFT JOIN staff_address a ON s.staff_id = a.staff_id
		WHERE s.deleted_at = '0' `+condition+`
		ORDER BY staff_id DESC
		LIMIT :limit
		OFFSET :offset`, queryParams)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff")
	}

	staffList := &staffPb.StaffList{}
	for rows.Next() {
		staff := staffPb.Staff{}
		err := rows.StructScan(&staff)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff")
		}

		unpackedUUID, err := crypto.Parse([]byte(staff.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for staff ID %d", staff.Id)
		}
		staff.Uuid = unpackedUUID.String()

		staffList.Staff = append(staffList.Staff, &staff)
	}

	logger.Infof("Found %d staff members.", len(staffList.Staff))

	if len(staffList.Staff) > 0 {
		staffIds := []int32{}
		for _, staff := range staffList.Staff {
			staffIds = append(staffIds, staff.Id)
		}

		// Bulk fetch the photos
		photosMap := map[int32][]*staffPb.StaffPhoto{}
		query, args, _ := sqlx.In("SELECT * FROM staff_photo WHERE staff_id IN (?) AND deleted_at = '0'", staffIds)
		query = s.DB.Rebind(query)
		rows2, err := s.DB.Queryx(query, args...)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff phones")
		}
		for rows2.Next() {
			photo := &staffPb.StaffPhoto{}
			err := rows2.StructScan(photo)
			if err != nil {
				return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff photos")
			}

			unpackedUUID, err := crypto.Parse([]byte(photo.Uuid))
			if err != nil {
				return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
			}
			photo.Uuid = unpackedUUID.String()

			if _, ok := photosMap[photo.StaffId]; !ok {
				photosMap[photo.StaffId] = []*staffPb.StaffPhoto{}
			}
			photosMap[photo.StaffId] = append(photosMap[photo.StaffId], photo)
		}
		for _, staff := range staffList.Staff {
			if _, ok := photosMap[staff.Id]; ok {
				staff.Photos = photosMap[staff.Id]
			} else {
				staff.Photos = []*staffPb.StaffPhoto{}
			}
		}

		// Bulk fetch the social profiles
		profilesMap := map[int32][]*staffPb.SocialProfile{}
		query, args, _ = sqlx.In("SELECT * FROM staff_social_profile WHERE staff_id IN (?) AND deleted_at = '0'", staffIds)
		query = s.DB.Rebind(query)
		rows2, err = s.DB.Queryx(query, args...)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff social profiles")
		}
		for rows2.Next() {
			profile := &staffPb.SocialProfile{}
			err := rows2.StructScan(profile)
			if err != nil {
				return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff social profile")
			}

			if _, ok := profilesMap[profile.StaffId]; !ok {
				profilesMap[profile.StaffId] = []*staffPb.SocialProfile{}
			}
			profilesMap[profile.StaffId] = append(profilesMap[profile.StaffId], profile)
		}
		for _, staff := range staffList.Staff {
			if _, ok := profilesMap[staff.Id]; ok {
				staff.SocialProfiles = profilesMap[staff.Id]
			} else {
				staff.SocialProfiles = []*staffPb.SocialProfile{}
			}
		}

		// Bulk fetch the phone numbers
		phonesMap := map[int32][]*staffPb.StaffPhone{}
		query, args, _ = sqlx.In(`SELECT
				sp.staff_id,
				sp.staff_phone_id,
				spt.name AS type_name,
				sp.number
			FROM staff_phone sp
			JOIN staff_phone_type spt ON sp.staff_phone_type_id = spt.staff_phone_type_id
			WHERE sp.staff_id IN (?)`, staffIds)
		query = s.DB.Rebind(query)
		rows2, err = s.DB.Queryx(query, args...)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff phones")
		}
		for rows2.Next() {
			phone := &staffPb.StaffPhone{}
			err := rows2.StructScan(phone)
			if err != nil {
				return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff phone")
			}

			if _, ok := phonesMap[phone.StaffId]; !ok {
				phonesMap[phone.StaffId] = []*staffPb.StaffPhone{}
			}
			phonesMap[phone.StaffId] = append(phonesMap[phone.StaffId], phone)
		}
		for _, staff := range staffList.Staff {
			if _, ok := phonesMap[staff.Id]; ok {
				staff.Phones = phonesMap[staff.Id]
			} else {
				staff.Phones = []*staffPb.StaffPhone{}
			}
		}
	}

	return staffList, nil
}

// GetStaffBySlug fetches a staff by slug
func (s *Server) GetStaffBySlug(ctx context.Context, req *staffPb.GetBySlugRequest) (*staffPb.Staff, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetStaffBySlug: %v", req)
	defer timeTrack(logger, time.Now(), "GetStaffBySlug")

	// Validations
	req.Slug = strings.TrimSpace(req.Slug)
	if req.Slug == "" {
		logger.Errorf("The slug is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The slug is required.")
	}

	var staff staffPb.Staff
	err := s.DB.Get(&staff, `SELECT
			s.*,
			sd.featured,
			sd.quote,
			a.address "staff_address.address",
			a.address2 "staff_address.address2",
			a.city "staff_address.city",
			a.state "staff_address.state",
			a.country "staff_address.country",
			a.postal_code "staff_address.postal_code",
			a.foreign_zip "staff_address.foreign_zip"
		FROM staff s
		INNER JOIN staff_details sd ON s.staff_id = sd.staff_id
		LEFT JOIN staff_address a ON s.staff_id = a.staff_id
		WHERE s.deleted_at = '0' AND s.slug = ?`, req.Slug)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Errorf("Cannot find staff with the slug '%s'.", req.Slug)
			return nil, grpc.Errorf(codes.NotFound, "")
		}

		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff")
	}

	unpackedUUID, err := crypto.Parse([]byte(staff.Uuid))
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for staff ID %d", staff.Id)
	}
	staff.Uuid = unpackedUUID.String()

	// Fetch the photos
	rows, err := s.DB.Queryx("SELECT * FROM staff_photo WHERE staff_id = ? AND deleted_at = '0'", staff.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff phones")
	}
	for rows.Next() {
		photo := &staffPb.StaffPhoto{}
		err := rows.StructScan(photo)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff photos")
		}

		unpackedUUID, err := crypto.Parse([]byte(photo.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		photo.Uuid = unpackedUUID.String()

		staff.Photos = append(staff.Photos, photo)
	}

	// Fetch the social profiles
	rows, err = s.DB.Queryx("SELECT * FROM staff_social_profile WHERE staff_id = ? AND deleted_at = '0'", staff.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff social profiles")
	}
	for rows.Next() {
		profile := &staffPb.SocialProfile{}
		err := rows.StructScan(profile)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff social profile")
		}

		staff.SocialProfiles = append(staff.SocialProfiles, profile)
	}

	// Fetch the phone numbers
	rows, err = s.DB.Queryx(`SELECT
			sp.staff_id,
			sp.staff_phone_id,
			spt.name AS type_name,
			sp.number
		FROM staff_phone sp
		JOIN staff_phone_type spt ON sp.staff_phone_type_id = spt.staff_phone_type_id
		WHERE sp.staff_id = ?`, staff.Id)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff phones")
	}
	for rows.Next() {
		phone := &staffPb.StaffPhone{}
		err := rows.StructScan(phone)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff phone")
		}

		staff.Phones = append(staff.Phones, phone)
	}

	return &staff, nil
}

// UpdateStaff updates a Staff
func (s *Server) UpdateStaff(ctx context.Context, req *staffPb.Staff) (*staffPb.Staff, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("UpdateStaff: %v", req)
	defer timeTrack(logger, time.Now(), "UpdateStaff")

	if req.Uuid == "" {
		logger.Error("The staff UUID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff UUID is required.")
	}
	req.Name = strings.TrimSpace(req.Name)
	if req.Name == "" {
		logger.Error("The staff name is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff name is required.")
	}
	if req.Address == nil {
		logger.Error("The staff address is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff address is required.")
	}
	// Validate the phones
	if req.Phones != nil {
		for _, ph := range req.Phones {
			ph.TypeName = strings.ToLower(ph.TypeName)

			if _, ok := PhoneType2DBID[ph.TypeName]; !ok {
				logger.Errorf("Invalid phone type '%s'.", ph.TypeName)
				return nil, grpc.Errorf(codes.InvalidArgument, "Invalid phone type '%s'.", ph.TypeName)
			}
		}
	}
	// Validate the social profiles
	if req.SocialProfiles != nil {
		for _, reqSocialProfile := range req.SocialProfiles {
			if !slices.Scontains(SocialProfileTypes, reqSocialProfile.Type) {
				logger.Errorf("Invalid social profile type '%s'.", reqSocialProfile.Type)
				return nil, grpc.Errorf(codes.InvalidArgument, "Invalid social profile type '%s'.", reqSocialProfile.Type)
			}
		}
	}

	tx := s.DB.MustBegin()

	// update the staff
	_, err := tx.NamedExec(`UPDATE staff s
		INNER JOIN staff_details sd ON sd.staff_id = s.staff_id
		SET s.name = :name, s.fname = :fname, s.lname = :lname, s.bio = :bio, s.gender = :gender,
			s.image_url = :image_url, s.email = :email, s.multi_location = :multi_location, s.appointment_trn = :appointment_trn,
			s.reservation_trn = :reservation_trn, s.independent_contractor = :independent_contractor,
			s.always_allow_double_booking = :always_allow_double_booking, s.user_access_level = :user_access_level,
			s.is_approved = :is_approved,
			sd.featured = CASE WHEN :featured = -1 THEN sd.featured ELSE :featured END,
			sd.quote = CASE WHEN :quote = '-' THEN sd.quote ELSE :quote END
		WHERE s.deleted_at = '0' AND s.uuid = UNHEX(REPLACE(:uuid,'-',''))`, req)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}

	logger.Debugf("Updated staff '%s'.", req.GetUuid())

	// Get the updated staff
	var staff staffPb.Staff
	err = tx.Get(&staff, `SELECT
			s.*,
			sd.featured,
			sd.quote,
			a.address "staff_address.address",
			a.address2 "staff_address.address2",
			a.city "staff_address.city",
			a.state "staff_address.state",
			a.country "staff_address.country",
			a.postal_code "staff_address.postal_code",
			a.foreign_zip "staff_address.foreign_zip"
		FROM staff AS s
		INNER JOIN staff_details sd ON sd.staff_id = s.staff_id
		LEFT JOIN staff_address a ON s.staff_id = a.staff_id
		WHERE s.uuid = UNHEX(REPLACE(?,'-',''))
			AND s.deleted_at = '0'`, req.Uuid)
	if err != nil {
		tx.Rollback()

		if err == sql.ErrNoRows {
			logger.Error("Cannot find the staff to be updated.")
			return nil, grpc.Errorf(codes.NotFound, "Cannot find the staff to be updated")
		}
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the updated staff")
	}
	staff.Uuid = req.Uuid

	// Update the address
	req.Address.StaffId = staff.Id
	_, err = tx.NamedExec(`UPDATE staff_address
		SET address = :address, address2 = :address2, city = :city, state = :state, country = :country,
			postal_code = :postal_code, foreign_zip = :foreign_zip
		WHERE staff_id = :staff_id`, req.Address)
	if err != nil {
		tx.Rollback()
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database for address update.")
	}
	staff.Address = req.Address
	logger.Debugf("Updated address for '%s'.", req.GetUuid())

	// Handle the phones
	if req.Phones != nil {
		_, err = tx.Exec("DELETE FROM staff_phone WHERE staff_id = ?", staff.Id)
		if err != nil {
			tx.Rollback()
			return nil, grpcUtils.InternalError(logger, err, "Unable to query database for phones removal.")
		}
		logger.Debugf("Removed old phones for '%s'.", req.Uuid)

		for _, ph := range req.Phones {
			ph.StaffId = staff.Id
			ph.TypeName = strings.ToLower(ph.TypeName)

			_, err = tx.NamedExec(`INSERT INTO staff_phone
					(staff_id, staff_phone_type_id, number)
				VALUES (:staff_id, (SELECT staff_phone_type_id FROM staff_phone_type WHERE name = :type_name), :number)`, ph)
			if err != nil {
				tx.Rollback()

				logger.Errorf("Cannot create database entry for phone: %v", err)
				return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry for phone.")
			}
		}
		logger.Debugf("Created new phone(s) for '%s'.", req.GetUuid())

		staff.Phones = req.Phones
	} else {
		// Fetch the phones if we didnt get them in the request
		staff.Phones = []*staffPb.StaffPhone{}

		phones := []staffPb.StaffPhone{}
		err = s.DB.Select(&phones, `SELECT
				sp.staff_phone_id,
				spt.name AS type_name,
				sp.number
			FROM staff_phone sp
			JOIN staff_phone_type spt ON sp.staff_phone_type_id = spt.staff_phone_type_id
			WHERE sp.staff_id = ?`, staff.Id)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while fetching phones")
		}
		for _, ph := range phones {
			dph := ph
			staff.Phones = append(staff.Phones, &dph)
		}
	}

	// Update the social profiles
	if req.SocialProfiles != nil {
		socialProfiles := []staffPb.SocialProfile{}
		err = tx.Select(&socialProfiles, `SELECT * FROM staff_social_profile WHERE deleted_at = '0' AND staff_id = ?`, staff.Id)
		if err != nil {
			tx.Rollback()
			return nil, grpcUtils.InternalError(logger, err, "Error while fetching the social profiles")
		}

		// Delete the profile if it was removed
		foundIndices := []int32{}
		for _, dbProfile := range socialProfiles {
			shouldRemove := true
			for idx, reqProfile := range req.SocialProfiles {
				if dbProfile.Type == reqProfile.Type && reqProfile.Url == dbProfile.Url {
					foundIndices = append(foundIndices, int32(idx))
					shouldRemove = false
					break
				}
			}

			// Remove if not found
			if shouldRemove {
				_, err = tx.Exec(`UPDATE staff_social_profile SET deleted_at = CURRENT_TIMESTAMP
					WHERE deleted_at = '0' AND staff_social_profile_id = ?`, dbProfile.Id)
				if err != nil {
					tx.Rollback()
					return nil, grpcUtils.InternalError(logger, err, "Error while deleting a social profile")
				}
			}
		}

		// Add profiles
		for idx, reqProfile := range req.SocialProfiles {
			// Add only if we didn't already find them in DB
			if !slices.Icontains32(foundIndices, int32(idx)) {
				reqProfile.StaffId = staff.Id

				_, err = tx.NamedExec(`INSERT INTO staff_social_profile (staff_id, type, url)
					VALUES (:staff_id, :type, :url)`, reqProfile)
				if err != nil {
					tx.Rollback()
					return nil, grpcUtils.InternalError(logger, err, "Error while adding a new social profile")
				}
			}
		}

		staff.SocialProfiles = req.SocialProfiles
	} else {
		// We need to fetch them from DB if we don't get them in the request
		staff.SocialProfiles = []*staffPb.SocialProfile{}

		socialProfiles := []staffPb.SocialProfile{}
		err = s.DB.Select(&socialProfiles, `SELECT * FROM staff_social_profile WHERE staff_id = ? AND deleted_at = '0'`, staff.Id)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while fetching the social profiles")
		}
		for _, sp := range socialProfiles {
			sph := sp
			staff.SocialProfiles = append(staff.SocialProfiles, &sph)
		}
	}

	tx.Commit()

	return &staff, nil
}

// DeleteStaff removes a Staff (sets deleted_at)
func (s *Server) DeleteStaff(ctx context.Context, req *staffPb.DeleteStaffRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteStaff: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteStaff")

	if req.Uuid == "" {
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff UUID is required.")
	}

	result, err := s.DB.Exec("UPDATE staff SET deleted_at = CURRENT_TIMESTAMP WHERE deleted_at = '0' AND uuid = UNHEX(REPLACE(?,'-',''))", req.Uuid)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Unable to query database.")
	}
	if no, _ := result.RowsAffected(); no == 0 {
		logger.Errorf("DeleteStaff - staff not found: %s", req.Uuid)
		return nil, grpc.Errorf(codes.NotFound, "")
	}

	return &empty.Empty{}, nil
}

// GetStaffBySite fetches all the staff members of the given site ID
func (s *Server) GetStaffBySite(ctx context.Context, req *staffPb.GetStaffBySiteRequest) (*staffPb.StaffList, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetStaffBySite: %v", req)
	defer timeTrack(logger, time.Now(), "GetStaffBySite")

	if req.SiteId < 1 {
		logger.Error("The site ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The site ID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT
			s.*,
			sd.featured,
			sd.quote,
			a.address "staff_address.address",
			a.address2 "staff_address.address2",
			a.city "staff_address.city",
			a.state "staff_address.state",
			a.country "staff_address.country",
			a.postal_code "staff_address.postal_code",
			a.foreign_zip "staff_address.foreign_zip"
		FROM staff AS s
		INNER JOIN staff_details sd ON sd.staff_id = s.staff_id
		LEFT JOIN staff_address a ON s.staff_id = a.staff_id
		WHERE s.site_id = ? AND s.deleted_at = '0'`, req.SiteId)
	if err != nil {
		logger.Errorf("Error while fetching the staff members: %v", err)
		return nil, err
	}

	staffList := &staffPb.StaffList{}
	for rows.Next() {
		staff := staffPb.Staff{}
		err := rows.StructScan(&staff)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff")
		}

		unpackedUUID, err := crypto.Parse([]byte(staff.Uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
		}
		staff.Uuid = unpackedUUID.String()

		staffList.Staff = append(staffList.Staff, &staff)
	}

	// Bulk fetch the photos
	if len(staffList.Staff) > 0 {
		staffIds := []int32{}
		photosMap := map[int32][]*staffPb.StaffPhoto{}
		for _, staff := range staffList.Staff {
			staffIds = append(staffIds, staff.Id)
		}
		query, args, _ := sqlx.In("SELECT * FROM staff_photo WHERE staff_id IN (?) AND deleted_at = '0'", staffIds)
		query = s.DB.Rebind(query)
		rows2, err := s.DB.Queryx(query, args...)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff phones")
		}
		for rows2.Next() {
			photo := &staffPb.StaffPhoto{}
			err := rows2.StructScan(photo)
			if err != nil {
				return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff photos")
			}

			unpackedUUID, err := crypto.Parse([]byte(photo.Uuid))
			if err != nil {
				return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID")
			}
			photo.Uuid = unpackedUUID.String()

			if _, ok := photosMap[photo.StaffId]; !ok {
				photosMap[photo.StaffId] = []*staffPb.StaffPhoto{}
			}
			photosMap[photo.StaffId] = append(photosMap[photo.StaffId], photo)
		}
		for _, staff := range staffList.Staff {
			if _, ok := photosMap[staff.Id]; ok {
				staff.Photos = photosMap[staff.Id]
			} else {
				staff.Photos = []*staffPb.StaffPhoto{}
			}
		}
	}

	return staffList, nil
}

// SyncStaffFromDBToES syncs the given staff by UUIDs from DB to ES
func (s *Server) SyncStaffFromDBToES(ctx context.Context, req *staffPb.SyncStaffFromDBToESRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("SyncStaffFromDBToES: %v", req)
	defer timeTrack(logger, time.Now(), "SyncStaffFromDBToES")

	req.Uuids = strings.TrimSpace(req.Uuids)
	if len(req.Uuids) > 0 {
		_, err := s.SyncService.SyncStaffFromDB(strings.Split(req.Uuids, ","))
		if err != nil {
			logger.Errorf("Error while syncing the staff: %v", err)
			return nil, grpcUtils.InternalError(logger, err, "Error while syncing the staff")
		}
	}

	return &empty.Empty{}, nil
}

// CreateStaffFavourite creates a new StaffFavourite
func (s *Server) CreateStaffFavourite(ctx context.Context, req *staffPb.StaffFavouriteRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("CreateStaffFavourite: %v", req)
	defer timeTrack(logger, time.Now(), "CreateStaffFavourite")

	// Validations
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	if req.StaffId < 1 {
		logger.Errorf("The staff ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff ID is required.")
	}

	_, err := s.DB.Exec(`INSERT IGNORE INTO staff_favourite (staff_id, account_id) VALUES (?, ?)`, req.StaffId, req.AccountId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot create database entry.")
	}

	logger.Debugf("Created new staff favourite for staff %d and account %d.", req.StaffId, req.AccountId)

	return &empty.Empty{}, nil
}

// DeleteStaffFavourite removes a StaffFavourite
func (s *Server) DeleteStaffFavourite(ctx context.Context, req *staffPb.StaffFavouriteRequest) (*empty.Empty, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("DeleteStaffFavourite: %v", req)
	defer timeTrack(logger, time.Now(), "DeleteStaffFavourite")

	// Validations
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}
	if req.StaffId < 1 {
		logger.Errorf("The staff ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The staff ID is required.")
	}

	_, err := s.DB.Exec(`DELETE FROM staff_favourite WHERE staff_id = ? AND account_id = ?`, req.StaffId, req.AccountId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Cannot delete database entry.")
	}

	logger.Debugf("Removed staff favourite for staff %d and account %d.", req.StaffId, req.AccountId)

	return &empty.Empty{}, nil
}

// GetStaffFavouriteForAccount removes a StaffFavourite
func (s *Server) GetStaffFavouriteForAccount(ctx context.Context, req *staffPb.GetStaffFavouriteForAccountRequest) (*staffPb.GetStaffFavouriteForAccountResponse, error) {
	logger := grpcUtils.GetContextLogger(ctx, s.Logger)

	logger.Infof("GetStaffFavouriteForAccount: %v", req)
	defer timeTrack(logger, time.Now(), "GetStaffFavouriteForAccount")

	// Validations
	if req.AccountId < 1 {
		logger.Errorf("The account ID is required.")
		return nil, grpc.Errorf(codes.InvalidArgument, "The account ID is required.")
	}

	rows, err := s.DB.Queryx(`SELECT s.uuid
		FROM staff_favourite sf
		INNER JOIN staff s ON sf.staff_id = s.staff_id
		WHERE s.deleted_at = '0' AND sf.account_id = ?
		ORDER BY sf.created_at DESC`, req.AccountId)
	if err != nil {
		return nil, grpcUtils.InternalError(logger, err, "Error while fetching the staff favourites")
	}

	staffUuids := []string{}
	for rows.Next() {
		var uuid string

		err := rows.Scan(&uuid)
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Error while scanning the staff favourites")
		}

		unpackedUUID, err := crypto.Parse([]byte(uuid))
		if err != nil {
			return nil, grpcUtils.InternalError(logger, err, "Cannot unpack the UUID for staff favourite.")
		}
		uuid = unpackedUUID.String()

		staffUuids = append(staffUuids, uuid)
	}

	return &staffPb.GetStaffFavouriteForAccountResponse{StaffUuids: staffUuids}, nil
}
