package staff

import (
	"fmt"

	staffPb "bitbucket.org/canopei/staff/protobuf"
	"google.golang.org/grpc"
)

// NewClient returns a gRPC client for interacting with the staff service.
func NewClient(addr string) (staffPb.StaffServiceClient, func() error, error) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, fmt.Errorf("did not connect: %v", err)
	}

	return staffPb.NewStaffServiceClient(conn), conn.Close, nil
}
