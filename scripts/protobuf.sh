#!/bin/sh
set -e

cd ./protobuf

echo "Generating the files for service 'staff'..."

# pb
protoc \
    -I .\
    -I ../vendor \
    -I ../vendor/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
    --gogo_out=Mgoogle/api/annotations.proto=github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis/google/api,plugins=grpc:. \
    staff.proto

# gateway
protoc \
    -I .\
    -I ../vendor \
    -I ../vendor/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
    --grpc-gateway_out=logtostderr=true:. \
    staff.proto

# we don't want to omit empties in JSON, except for the ones we really want (omitempty!)
sed -i 's/,omitempty!/,omt!/g' *.pb.go
sed -i 's/,omitempty//g' *.pb.go
sed -i 's/omt!/omitempty/g' *.pb.go

echo "Done."